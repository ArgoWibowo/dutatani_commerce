<?php
include "headers/headerguest.php";
?>
  <style type="text/css">
  /* just for jsfiddle, next/prev arrows on carousel */
  .middle-indicator{
   position:absolute;
   top:50%;
   }
  .middle-indicator-text{
   font-size: 4.2rem;
  }
  a.middle-indicator-text{
    color:white !important;
  }
  .content-indicator{
    width: 64px;
    height: 64px;
    background: none;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px; 
  }
    .indicators{
    visibility: hidden;
  }
  img.items{
    position: relative;
    float: left;
    width: 100%;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
  }
  a.cat{
    color: #999;
  }
  a.cat:hover{
    border-bottom: 2px solid black;
    color: #000 ;
  }
  a.catactive{
    border-bottom: 0;
    color: #000;
  }
  a.itemname{
    border-bottom: 2px solid black;
    color: #000;
  }
  a.itemname:hover{
    border-bottom: 0;
    color: #999;
  }
  </style>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
  <div class="carousel carousel-slider center" data-indicators="true">
    <div class="carousel-fixed-item center middle-indicator">
     <div class="left">
      <a href="Previo" class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="material-icons left  middle-indicator-text">chevron_left</i></a>
     </div>
     
     <div class="right">
     <a href="Siguiente" class="moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="material-icons right middle-indicator-text">chevron_right</i></a>
     </div>
    </div>
    <div class="carousel-item" href="#one!">
      <img src="pictures/hoe.jpg">
    </div>
    <div class="carousel-item amber white-text" href="#two!">
      <img src="pictures/gardentools.jpg">
    </div>
    <div class="carousel-item green white-text" href="#three!">
      <img src="pictures/axe.jpg">
    </div>
  </div>


    </div>
  </div>

  <br><br>
  <div class="container">
    <div class="section">
      <!--   Icon Section   -->
  <h3>Tentang Dodolantani</h3>
  <p>Halo sahabat yang luar biasa, pada kesempatan kali ini izinkan kami dari Dodolantani untuk memperkenalkan diri. Mudah-mudahan dengan adanya perkenalan ini sahabat-sahabat sekalian akan lebih senang berbelanja di toko online ini.

  Sungguh terhormat bagi kami, jika Anda datang ke toko ini dan bisa memperoleh banyak hal yang bermanfaat.

  Dodolantani adalah sebuah toko yang menyediakan alat, bahan, dan hasil produk pertanian. Bermula dari sebuah bangunan yang berlokasi di tempat yang biasa saja, toko kami akhirnya bisa menjangkau pasar yang lebih luas lagi melalui internet.

  Seiring dengan semakin berkembangnya teknologi, maka kami mencoba berinovasi dalam berbisnis. Salah satu bentuk inovasinya adalah dengan menyediakan berbagai layanan toko kami melalui media internet, dan toko online ini adalah salah satu bentuk pelayanan kami kepada Anda.

  Keberadaan dari toko online ini diharapkan dapat mempermudah Anda untuk mendapatkan berbagai produk berkualitas tinggi namun dengan harga yang terjangkau.

  Dalam memberi layanan, kami selalu mencoba memberi persembahan terbaik kepada siapapun. Selain itu kami juga selalu menjunjung tinggi nilai-nilai etika yang baik, seperti kejujuran, ketepatan, dan profesionalitas dalam berbisnis. Mudah-mudahan dengannya toko online kami bisa memberi banyak manfaat bagi Anda.

  Sekian dulu perkenalan ini. Semoga perkenalan ini bisa memberi inspirasi dan manfaat bagi Anda.

  Salam super dan semoga Anda sukses selalu.</p>
  
  <p>Halo sahabat yang luar biasa, pada kesempatan kali ini izinkan kami dari Dodolantani untuk memperkenalkan diri. Mudah-mudahan dengan adanya perkenalan ini sahabat-sahabat sekalian akan lebih senang berbelanja di toko online ini.

  Sungguh terhormat bagi kami, jika Anda datang ke toko ini dan bisa memperoleh banyak hal yang bermanfaat.

  Dodolantani adalah sebuah toko yang menyediakan alat, bahan, dan hasil produk pertanian. Bermula dari sebuah bangunan yang berlokasi di tempat yang biasa saja, toko kami akhirnya bisa menjangkau pasar yang lebih luas lagi melalui internet.

  Seiring dengan semakin berkembangnya teknologi, maka kami mencoba berinovasi dalam berbisnis. Salah satu bentuk inovasinya adalah dengan menyediakan berbagai layanan toko kami melalui media internet, dan toko online ini adalah salah satu bentuk pelayanan kami kepada Anda.

  Keberadaan dari toko online ini diharapkan dapat mempermudah Anda untuk mendapatkan berbagai produk berkualitas tinggi namun dengan harga yang terjangkau.

  Dalam memberi layanan, kami selalu mencoba memberi persembahan terbaik kepada siapapun. Selain itu kami juga selalu menjunjung tinggi nilai-nilai etika yang baik, seperti kejujuran, ketepatan, dan profesionalitas dalam berbisnis. Mudah-mudahan dengannya toko online kami bisa memberi banyak manfaat bagi Anda.

  Sekian dulu perkenalan ini. Semoga perkenalan ini bisa memberi inspirasi dan manfaat bagi Anda.

  Salam super dan semoga Anda sukses selalu.</p>

  <p>Halo sahabat yang luar biasa, pada kesempatan kali ini izinkan kami dari Dodolantani untuk memperkenalkan diri. Mudah-mudahan dengan adanya perkenalan ini sahabat-sahabat sekalian akan lebih senang berbelanja di toko online ini.

  Sungguh terhormat bagi kami, jika Anda datang ke toko ini dan bisa memperoleh banyak hal yang bermanfaat.

  Dodolantani adalah sebuah toko yang menyediakan alat, bahan, dan hasil produk pertanian. Bermula dari sebuah bangunan yang berlokasi di tempat yang biasa saja, toko kami akhirnya bisa menjangkau pasar yang lebih luas lagi melalui internet.

  Seiring dengan semakin berkembangnya teknologi, maka kami mencoba berinovasi dalam berbisnis. Salah satu bentuk inovasinya adalah dengan menyediakan berbagai layanan toko kami melalui media internet, dan toko online ini adalah salah satu bentuk pelayanan kami kepada Anda.

  Keberadaan dari toko online ini diharapkan dapat mempermudah Anda untuk mendapatkan berbagai produk berkualitas tinggi namun dengan harga yang terjangkau.

  Dalam memberi layanan, kami selalu mencoba memberi persembahan terbaik kepada siapapun. Selain itu kami juga selalu menjunjung tinggi nilai-nilai etika yang baik, seperti kejujuran, ketepatan, dan profesionalitas dalam berbisnis. Mudah-mudahan dengannya toko online kami bisa memberi banyak manfaat bagi Anda.

  Sekian dulu perkenalan ini. Semoga perkenalan ini bisa memberi inspirasi dan manfaat bagi Anda.

  Salam super dan semoga Anda sukses selalu.</p>
    
    </div>


  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          


        </div>
        
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script type="text/javascript">
  // carousel initializing & autoplay
    $(document).ready(function(){
      $('.carousel').carousel();
      autoplay()   
      function autoplay() {
          $('.carousel').carousel('next');
          setTimeout(autoplay, 4500);
      }
    });
   // start carrousel
   $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: false
   });


   // move next carousel
   $('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
   });

   // move prev carousel
   $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
   });
   $(document).ready(function() {
      $(".button-collapse").sideNav();
      $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    });
    });
  </script>
  </body>
</html>
