<?php
session_start();
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  include "../headers/headerlogged.php";
}else{
  include "../headers/headerguest2.php";
}
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  if($tipe!=2)
  {
    header("Location:../admin/home.php");
  }
}else{
  header("Location:../index.php");
}

$tampung = 0;
?>
<!DOCTYPE html>
<html lang="en">
 <div class="container">
    <div class="section"> 

      <!--   Side bar   -->
      <div class="row" style="background-color:#f5f5f5;">
        <div class="col s12 m10 offset-m1">
          <br>
          <div class="icon-block">
            <center>
              <b>Jumlah yang Harus Dibayar</b>
              <h5><?php echo 'Rp '.number_format ($_GET['byr']); ?></h5>
              <a href="#modal1" class="modal-trigger">Lihat Detail</a><br><br> 
              <br><br> 
              <img src="../pictures/bca.png" width="150">
            </center>
              <p class="light">
              1. Masukkan Kartu ATM BCA & PIN<br>
              2. Pilih menu Transaksi Lainnya > Transfer > ke 

              Rekening BCA Virtual Account<br>
              3. Masukkan 5 angka kode perusahaan untuk Tokopedia 

              (80777) dan Nomor HP yang terdaftar di akun Tokopedia 

              Anda (Contoh: 80777081219299279)<br>
              4. Di halaman konfirmasi, pastikan detil pembayaran 

              sudah sesuai seperti No VA, Nama, Perus/Produk dan 

              Total Tagihan<br>
              5. Masukkan Jumlah Transfer sesuai dengan Total 

              Tagihan<br>
              6. Ikuti instruksi untuk menyelesaikan transaksi<br>
              7. Simpan struk transaksi sebagai bukti pembayaran<br>
              8. Batas Waktu Pembayaran 3 Hari Jika melebihi batas maka akan dianggap batal membeli<br>
              </p>
              <br><br> 
            
          </div>
          <br><br>
        </div>
        <br>
      </div>
<center> <a class="waves-effect waves-light btn modal-trigger" href="#modal2">Upload Bukti Bayar</a> </center>
    </div>
    </div>
    </div>

  <br><br>
  

  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">
      <h4>Daftar Item Dibayar</h4>
      <table class="striped responsive-table">
        <?php 
          $items = mysql_query("select * from trans_permintaan where ID_User='".$un."' AND Status='Belum Dibayar' AND ID_Permintaan='".$_GET['id']."'");
          while ($item = mysql_fetch_array($items)){
            $get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$item['ID_Penawaran']."'");
            $barang = mysql_fetch_array($get_data);
            $cekkat = substr($barang['ID'],0,3);
              if($cekkat=="BAH")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Bahan'];
                $path="bahan";
              }
              elseif($cekkat=="HAS")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Hasil'];
                $path="produk";
              }
              elseif($cekkat=="ALA")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Alat'];
                $path="alat";
              };
              $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$barang['ID_User']}'");
              $ambiluser = mysql_fetch_array($ambilusert);
              $namauser=$ambiluser['nama'];

              $ambilkurir = mysql_query("SELECT * FROM kurir WHERE kode_kurir='{$item['kode_kurir']}'");
              $kurir = mysql_fetch_array($ambilkurir);

              $ambilalamat = mysql_query("SELECT * FROM detail_alamat WHERE kode_alamat='{$item['kode_alamat']}'");
              $alamat = mysql_fetch_array($ambilalamat);
      
      ?>

        <tr>
          <td width="9%" style="vertical-align: top;">
            <img src="../pictures/items/<?php echo $path?>/<?php echo $barang['Gambar1']?>" class="responsive-img float" width="80px" height="auto">
          </td>
          <td width="91%">
            <b><?php echo $nama; ?></b><br>
            <p class="light">Harga Satuan : <?php echo 'Rp '.number_format($barang['Harga']); ?></p> 
            <p class="light">Jumlah Beli : <?php echo $item['Qty']; ?></p>
            <p class="light">Ongkos Kirim : <?php echo 'Rp '.number_format($kurir['harga']); ?></p> 
          </td>
          
        </tr>

        <?php 
        $stoksekarang = $barang['Stok'] - $item['Qty'];
      } ?>
      </table>
    </div>
    
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Tutup</a>
    </div>
  </div>

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>

<!--modal-->
<center>
           
            <div id="modal2" class="modal modal-fixed-footer" >
              
              <form class="col s12" action="func/adduploadbukti.php?id=<?php echo $_GET['id']; ?>&byr=<?php echo $_GET['byr']; ?>&stoksekarang=<?php echo $stoksekarang; ?>" method="post" enctype="multipart/form-data">
              <div class="modal-content">
              <h4>Isi Data Pembayaran </h4>              
              <br>
                <div class="row">
                  <div class="input-field col s5">
                    <input placeholder="Contoh : 3654987521" id="No_Rekening" name="No_Rekening" type="text" class="validate">
                    <label for="first_name">No Rekening</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s5">
                    <input placeholder="Contoh : Novan Andriyanto" id="Nama_Rekening" name="Nama_Rekening" type="text" class="validate">
                    <label for="first_name">Nama Pengirim</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s5">
                    <input placeholder="Contoh : 150000" id="Jumlah_Pembayaran" name="Jumlah_Pembayaran" type="text" class="validate">
                    <label for="first_name">Jumlah Kirim</label>
                  </div>
                </div>
                <div class="input-field col s6">
                  <p style="color:#999;">Bukti Pembayaran</p>
                    <div class="file-field input-field">
                      <div class="btn orange lighten-2">
                        <span>File</span>
                        <input type="file" id="Bukti_Pembayaran" name="Bukti_Pembayaran">
                      </div>
                     
                      <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                      </div>
                    </div>
                  </div>
                </div>
              <div class="modal-footer">
              <input type="submit" value="Kirim" class="waves-effect waves-light btn modal-trigger"/>
              </div>
              </form>
            </div>
            </center>

  <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $('.tooltipped').tooltip({delay: 50});
      $('.modal').modal();
    });
    </script>
  </body>
</html>
