<?php
session_start();
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  include "headers/headerlogged2.php";
}else{
  include "headers/headerguest.php";
}

mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");

if(isset($_GET['keyword'])){ 
  $key = $_GET['keyword'];
  if(isset($_GET['kate']))
  {
    $kate = $_GET['kate'];
  }
  else
  {
    $kate = 'semua';
  }
  if(isset($_GET['min']))
  {
    if($_GET['min']!='')
    {
      $min = $_GET['min'];
    }
    else
    {
      $min='0';
    }
  }
  else
  {
    $min = '0';
  }
  if(isset($_GET['max']))
  {
    if($_GET['max']!='')
    {
      $max = $_GET['max'];
    }
    else
    {
      $max = '99999999999';
    }
  }
  else
    $max = '99999999999';
}else{
  $kate = 'semua';
  $min = '0';
    $max = '99999999999';
  $key = "";
}

?>
  <style type="text/css">
  img.items{
    position: relative;
    float: left;
    width: 100%;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
  }
  a.cat{
    color: #999;
  }
  a.cat:hover{
    border-bottom: 2px solid black;
    color: #000 ;
  }
  a.catactive{
    border-bottom: 0;
    color: #000;
  }
  a.itemname{
    border-bottom: 2px solid black;
    color: #000;
  }
  a.itemname:hover{
    border-bottom: 0;
    color: #999;
  }
  table{
    line-height:3px;
  }
  </style>

  <br>
  <div class="container">
    <div class="section">
        <div class="row" style="border-bottom:3px solid #009688">
          <form action="search.php">
            <div class="input-field col s12 m5">
              <i class="black-text material-icons prefix">search</i>  
              <input id="search" type="search" name="keyword" required placeholder="Cari.." value="<?php if(isset($_GET['keyword'])){ echo $key; } ?>">
              <label>Kata Kunci</label>
            </div>
            <div class="input-field col s12 m2">
              <select name="kate">
                <?php if(isset($_GET['keyword'])){ echo '<option value="'.$kate.'">'.ucwords($kate).'</option>'; } ?>
                <option value="semua">Semua</option>
                <option value="alat">Alat</option>
                <option value="bahan">Bahan</option>
                <option value="hasil">Hasil Produk</option>
              </select>
              <label>Kategori</label>
            </div>
            <div class="input-field col s12 m2">
              <input type="text" name="min" placeholder="Harga Min.">
              <label>Harga Minimal</label>
            </div>
            <div class="input-field col s12 m2">
              <input type="text" name="max" placeholder="Harga Max.">
              <label>Harga Maksimal</label>
            </div>
            <div class="input-field col s12 m1">
              <input type="submit" class="btn" value="Cari">
            </div>
          </form>
        </div>

      <!--   Icon Section   -->
      <div class="row">
        <?php
          $nama="";
          $namauser="";
          $kabupaten="";
          $path="";
          $go= "n";
          if(($kate=="semua")&&($key<>"")){
            $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif' AND trans_harga_prod.Harga>=".$min." AND trans_harga_prod.Harga<=".$max);
            while ($brs = mysql_fetch_array($hasil)){
              $cekkat = substr($brs['ID'],0,3);
              if($cekkat=="HAS")
                {
                $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$brs['ID']}' AND (Nama_Hasil like '%$key%')");
                if(mysql_num_rows($ambilnamat)>0){
                  $ambilnama = mysql_fetch_array($ambilnamat);
                  $nama=$ambilnama['Nama_Hasil'];
                  $path="produk";
                  $go="y"; }
                  
              if($go=="y"){
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>

          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php $go= "n"; }}
      elseif($cekkat=="ALA")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$brs['ID']}' AND (Nama_Alat like '%$key%')");
                if(mysql_num_rows($ambilnamat)>0){
                  $ambilnama = mysql_fetch_array($ambilnamat);
                  $nama=$ambilnama['Nama_Alat'];
                  $path="alat";
                  $go="y"; }

              if($go=="y"){
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>

          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php $go= "n"; }}
        
if($cekkat=="BAH")
                {
                $ambilnamat = mysql_query("SELECT Nama_Bahan FROM master_bahan_tani WHERE (Nama_Bahan like '%$key%') AND ID_Bahan='{$brs['ID']}'");
                if(mysql_num_rows($ambilnamat)>0){
                  $nama=mysql_result($ambilnamat, 0);
                  $path="bahan";
                  $go="y"; }
                  
              if($go=="y"){
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>
          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser; ?>, <?php echo $kabupaten; ?>
              </p>
              </div>
              <br>
          </div>
        <?php $go= "n"; }}
      }}elseif($kate=="alat"){
          $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif'");
            while ($brs = mysql_fetch_array($hasil)){
              $cekkat = substr($brs['ID'],0,3);
              if($cekkat=="ALA")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$brs['ID']}' AND (Nama_Alat like '%$key%')");
                if(mysql_num_rows($ambilnamat)>0){
                  $ambilnama = mysql_fetch_array($ambilnamat);
                  $nama=$ambilnama['Nama_Alat'];
                  $path="alat";
                  $go="y"; }

              if($go=="y"){
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>

          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php $go= "n"; }}}}elseif($kate=="bahan")
              {
            $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif'");
              while ($brs = mysql_fetch_array($hasil)){
                $cekkat = substr($brs['ID'],0,3);
                if($cekkat=="BAH")
                {
                $ambilnamat = mysql_query("SELECT Nama_Bahan FROM master_bahan_tani WHERE (Nama_Bahan like '%$key%') AND ID_Bahan='{$brs['ID']}'");
                if(mysql_num_rows($ambilnamat)>0){
                  $nama=mysql_result($ambilnamat, 0);
                  $path="bahan";
                  $go="y"; }
                  
              if($go=="y"){
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>
          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser; ?>, <?php echo $kabupaten; ?>
              </p>
              </div>
              <br>
          </div>
        <?php $go= "n"; }}}}elseif($kate=="hasil")
              {
            $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif'");
              while ($brs = mysql_fetch_array($hasil)){
                $cekkat = substr($brs['ID'],0,3);
                if($cekkat=="HAS")
                {
                $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$brs['ID']}' AND (Nama_Hasil like '%$key%')");
                if(mysql_num_rows($ambilnamat)>0){
                  $ambilnama = mysql_fetch_array($ambilnamat);
                  $nama=$ambilnama['Nama_Hasil'];
                  $path="produk";
                  $go="y"; }
                  
              if($go=="y"){
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>
          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php $go= "n"; }}}} ?>                           
        </div>
      </div>
    </div>
    <br><br>
  </div>


  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $(".button-collapse").sideNav();
      $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    });
    });
  </script>
  </body>
</html>
