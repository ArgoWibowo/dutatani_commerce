<?php 

/*BLOCK FUNCTION EXCELL*/
function cleanDataExcel(&$str){
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if($str == 't') $str = 'TRUE';
    if($str == 'f') $str = 'FALSE';
    if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
        $str = "'$str";
    }
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}

function createExcel($filename,$array){
    // filename for download
    $filename = $filename.".xls";
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");    
    $flag = false;
    foreach($array as $row) {
        if(!$flag) {
            // display field/column names as first row
            echo implode("\t", array_keys($row)) . "\r\n";
            $flag = true;
        }
        array_walk($row, 'cleanDataExcel');
        echo implode("\t", array_values($row)) . "\r\n";
    }
    return;
}
/*END OF - BLOCK FUNCTION EXCELL*/

/*BLOCK FUNCTION SQL*/
$trans_permintaan=array();
mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");
$hasil=mysql_query("SELECT * FROM trans_permintaan JOIN trans_pembayaran ON trans_pembayaran.ID_Permintaan=trans_permintaan.ID_Permintaan WHERE Status='Sudah Dibayar' AND Status_Transaksi='Valid'");
while($row=mysql_fetch_array($hasil)){
$get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$row['ID_Penawaran']."'");
$barang = mysql_fetch_array($get_data);
$get_data1 = mysql_query("SELECT * FROM trans_pembayaran WHERE ID_Permintaan='".$row['ID_Permintaan']."'");
$barang1 = mysql_fetch_array($get_data1);
$cekkat = substr($barang['ID'],0,3);
  if($cekkat=="BAH")
  {
    $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
    $ambilnama = mysql_fetch_array($ambilnamat);
    $nama=$ambilnama['Nama_Bahan'];
    $path="bahan";
  }
  elseif($cekkat=="HAS")
  {
    $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
    $ambilnama = mysql_fetch_array($ambilnamat);
    $nama=$ambilnama['Nama_Hasil'];
    $path="produk";
  }
  elseif($cekkat=="ALA")
  {
    $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
    $ambilnama = mysql_fetch_array($ambilnamat);
    $nama=$ambilnama['Nama_Alat'];
    $path="alat";
  };
  $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$row['ID_User']}'");
  $ambiluser = mysql_fetch_array($ambilusert);
  $namauser=$ambiluser['nama'];
  ?>
<?php
                  

    $trans_permintaan[]=array(
        "TANGGAL PEMESANAN"=>$row['Tgl_Permintaan'],
        "ID PERMINTAAN"=>$row['ID_Permintaan'],
        "NAMA USER"=>$ambiluser['nama'],
        "NAMA BARANG"=>$nama,
        "QTY"=>$row['Qty'],
        "HARGA"=>$barang['Harga'],
        "JUMLAH PEMBAYARAN"=>$barang1['Jumlah_Pembayaran'],
        "STATUS"=>$row['Status']
    );    
}
/*END BLOCK FUNCTION SQL*/

/*TRANSFER KE SYSTEM*/
createExcel("trans_permintaan.xls",$trans_permintaan);
?>