<?php

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('SIGress');
$pdf->SetTitle('Laporan Bulanan');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 12);

// add a page
$pdf->AddPage();

// data utk laporan
mysql_connect("localhost", "root", "");
mysql_select_db("gressdb");

include '../../func/func-lapkeluar.php';



// create some HTML content
$html = '
<style>
table {
border-collapse: collapse;
border:1px solid black;
}
table, tr, td{
padding:5px;
}
tr, td{
padding:0px 0px 0px 15px;
}
tr, td, th{
border:1px solid black;
}
</style>
<div align="center"><h1>Laporan Bulan '.$bulan.' '.$tahun.'</h1>
<h4>Dicetak pada Tanggal : '.$tgl_skrg.'</h4></div><br><br>
&nbsp;<br>
<table style="border:1px solid black;">
<tr>
	<th bgcolor="#999999" width="95px" align="center"><b>Nama Bahan</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Stok Awal</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Stok Realita</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Pemakaian</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Pemakaian Normal</b></th>
	<th bgcolor="#999999" width="178px" align="center"><b>Kesimpulan Pemakaian</b></th>
</tr>
<tr>
	<td>Sabun</td>
	<td align="right">'.number_format($stokawal_sabun).'ml</td>
	<td align="right">'.number_format($stok_sabun).'ml</td>
	<td align="right">'.number_format($pemakaian_sabun).'ml</td>
	<td align="right">'.number_format($rec_sabun).'ml</td>
	<td>'.$kesimpulan_sabun.'</td>
</tr>
<tr>
	<td>Pewangi</td>
	<td align="right">'.number_format($stokawal_pewangi).'ml</td>
	<td align="right">'.number_format($stok_pewangi).'ml</td>
	<td align="right">'.number_format($pemakaian_pewangi).'ml</td>
	<td align="right">'.number_format($rec_pewangi).'ml</td>
	<td>'.$kesimpulan_pewangi.'</td>
</tr>
<tr>
	<td>Plastik</td>
	<td align="right">'.number_format($stokawal_plastik).'ml</td>
	<td align="right">'.number_format($stok_plastik).'ml</td>
	<td align="right">'.number_format($pemakaian_plastik).'ml</td>
	<td align="right">'.number_format($rec_plastik).'ml</td>
	<td>'.$kesimpulan_plastik.'</td>
</tr>
</table>

<br>
&nbsp;
<br><br>
&nbsp;
<br>
<table style="border:1px solid black;">
<tr>
	<th bgcolor="#999999" width="95px" align="center"><b>Nama Bahan</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Jumlah Restok</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Pemakaian Normal</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Pengeluaran Restok (Rp)</b></th>
	<th bgcolor="#999999" width="100px" align="center"><b>Pengeluaran Normal (Rp)</b></th>
	<th bgcolor="#999999" width="178px" align="center"><b>Kesimpulan Pengeluaran</b></th>
</tr>
<tr>
	<td>Sabun</td>
	<td align="right">'.number_format($restock_sabun_qty).'ml</td>
	<td align="right">'.number_format($rec_sabun).'ml</td>
	<td align="right">'.number_format($restock_sabun).'</td>
	<td align="right">'.number_format($rec_sabun_hrg).'</td>
	<td>'.$kesimpulan_sabun_hrg.'</td>
</tr>
<tr>
	<td>Pewangi</td>
	<td align="right">'.number_format($restock_pewangi_qty).'ml</td>
	<td align="right">'.number_format($rec_pewangi).'ml</td>
	<td align="right">'.number_format($restock_pewangi).'</td>
	<td align="right">'.number_format($rec_pewangi_hrg).'</td>
	<td>'.$kesimpulan_pewangi_hrg.'</td>
</tr>
<tr>
	<td>Plastik</td>
	<td align="right">'.number_format($restock_plastik_qty).'ml</td>
	<td align="right">'.number_format($rec_plastik).'ml</td>
	<td align="right">'.number_format($restock_plastik).'</td>
	<td align="right">'.number_format($rec_plastik_hrg).'</td>
	<td>'.$kesimpulan_plastik_hrg.'</td>
</tr>
<tr>
	<td bgcolor="#999999" align="right" colspan="3"><b>Total (Rp)</b></td>
	<td align="right">'.number_format($pengeluaran_res).'</td>
	<td align="right">'.number_format($pengeluaran_std).'</td>
	<td>'.$kesimpulan_hrg.'</td>
</tr>
</table>
<font color="red">!Pengeluaran Normal dihitung dari pemakaian bahan normal dikalikan harga bahan</font>
<br>
&nbsp;
<br><br>
&nbsp;
<br>
<table style="border:1px solid black;">
<tr>
	<th bgcolor="#999999" width="400px" align="center" colspan="2"><b>Pemasukan dan Pengeluaran</b></th>
</tr>
<tr>
	<td>Pemasukan dari transaksi</td>
	<td colspan="1">Rp '.number_format($pemasukan).'</td>
</tr>
<tr>
	<td>Pengeluaran dari restok</td>
	<td colspan="1">Rp '.number_format($pengeluaran_res).'</td>
</tr>
<tr>
	<td bgcolor="#999999"><b>Pemasukan - Pengeluaran</b></td>
	<td align="left">Rp '.number_format($pemasukan-$pengeluaran_res).'</td>
</tr>
</table>
'
;


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Laporan Bulan '.$bulan.' '.$tahun.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
