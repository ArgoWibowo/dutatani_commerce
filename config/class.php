<?php 
session_start();

$database = new mysqli("localhost","root","","iais_ukdw");
//$database = new mysqli("localhost","root","","dbpertanian");

// echo "<pre>";
// print_r($database);
// echo "</pre>";

class kategori
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi = $mysqli;
	}

	//MENAMPILKAN KATGEORI ADMIN
	function tampil_kategori()
	{
		$semuakategori = array();
		$ambilkategori = $this->koneksi->query("SELECT * FROM master_kategori_produk");
		while($getkategori = $ambilkategori->fetch_assoc())
		{
			$semuakategori[]= $getkategori;
		}
		return $semuakategori;
	}

	//SIMPAN KATEGORI ADMIN
	function simpan_kategori($idkat, $jnskat, $namakat)
	{
		$ambil = $this->koneksi->query("SELECT * FROM master_kategori_produk WHERE ID_Kategori='$idkat'");
		$hasil = $ambil->num_rows;

		if($hasil==1)
		{
			return 'Gagal';
		}
		else
		{
			$this->koneksi->query("INSERT INTO master_kategori_produk (ID_Kategori, Jenis_kategori, Nama_kategori) VALUES ('$idkat','$jnskat','$namakat')") or die(mysqli_error($this->koneksi));
			return 'Berhasil';
		}
	}

	//HAPUS KATEGORI ADMIN
	function hapus_kategori($id)
	{
		$this->koneksi->query("DELETE FROM master_kategori_produk  WHERE ID_Kategori='$id'");
	}

	//AMBIL KATEGORI ADMIN
	function ambil_kategori($id)
	{
		$ambil = $this->koneksi->query("SELECT * FROM master_kategori_produk WHERE ID_Kategori='$id'");
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	//UBAH KATEGORI ADMIN
	function ubah_kategori($jnskat, $namakat, $idkat)
	{
		$this->koneksi->query("UPDATE master_kategori_produk SET Jenis_kategori='$jnskat', Nama_kategori='$namakat' WHERE ID_Kategori='$idkat'");
	}
}
$kategori = new kategori ($database);


class produk
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi = $mysqli;
	}

	function matikan_produk($pro)
	{
		$this->koneksi->query("UPDATE trans_penawaran_prod_tani SET Status='Mati', Validasi_Admin='0' WHERE ID_Produk='$pro'")or die(mysqli_error($this->koneksi));
	}

	function aktifkan_produk($pro)
	{
		$this->koneksi->query("UPDATE trans_penawaran_prod_tani SET Status='Aktif', Validasi_Admin='1' WHERE ID_Produk='$pro'") or die(mysqli_error($this->koneksi));
	}

	//TAMPIL PRODUK PENJUAL
	function tampil_hasil_produk($pengguna)
	{
		$semuahasil = array();
		$ambilhasil = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_hasil_tani ON master_produk_tani_commerce.ID=master_hasil_tani.ID_Hasil JOIN master_spesies_tanaman ON master_hasil_tani.ID_Spesies=master_spesies_tanaman.ID_Spesies WHERE ID_User='$pengguna'");
		while ($gethasil = $ambilhasil->fetch_assoc())
		{
			$semuahasil[] = $gethasil;
		}
		return $semuahasil;
	}

	//TAMPIL ALAT TANI PENJUAL
	function tampil_produk_alat($pengguna)
	{
		$semuaalat = array();
		$ambilalat= $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_alat_tani ON master_produk_tani_commerce.ID=master_alat_tani.ID_Alat WHERE ID_User='$pengguna'");
		while ($getalat = $ambilalat->fetch_assoc())
		{
			$semuaalat[] = $getalat;
		}
		return $semuaalat;
	}

	//TAMPIL BAHAN TANI PENJUAL
	function tampil_produk_bahan($pengguna)
	{
		$semuabahan = array();
		$ambilbahan = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_bahan_tani ON master_produk_tani_commerce.ID=master_bahan_tani.ID_Bahan WHERE ID_User='$pengguna'");
		while ($getbahan = $ambilbahan->fetch_assoc())
		{
			$semuabahan[] = $getbahan;
		}
		return $semuabahan;
	}

	//TAMPIL PRODUK DIANTAR MUKA PENJUAL
	function tampil_penawaran_produk()
	{
		$semuaproduk = array();
		$ambilproduk = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani");
		while ($getproduk = $ambilproduk->fetch_assoc())
		{
			$semuaproduk[] = $getproduk;
		}
		return $semuaproduk;
	}

	function tampil_penawaran_produk_aktif()
	{
		$semuaproduk = array();
		$ambilproduk = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status_Produk='Aktif'");
		while ($getproduk = $ambilproduk->fetch_assoc())
		{
			$semuaproduk[] = $getproduk;
		}
		return $semuaproduk;
	}

	function tampil_penawaran_produk_kategori_aktif($id)
	{
		$semuaproduk = array();
		$ambilproduk = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Kategori='$id' AND Status='Aktif'");
		while ($getproduk = $ambilproduk->fetch_assoc())
		{
			$semuaproduk[] = $getproduk;
		}
		return $semuaproduk;
	}



	//TAMPIL PRODUK DIANTAR MUKA PENJUAL
	function tampil_penawaran_produk_user($id)
	{
		$semuaproduk = array();
		$ambilproduk = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani WHERE ID_User='$id'");
		while ($getproduk = $ambilproduk->fetch_assoc())
		{
			$semuaproduk[] = $getproduk;
		}
		return $semuaproduk;
	}


	//SIMPAN PRODUK ADMIN PEJUAL
	function simpan_hasil_produk($Nama_Hasil, $ID_Spesies, $Deskripsi_Hasil, $hargarendah, $hargatinggi, $masaexpayet, $hargapas, $Satuan, $ID_Kategori)
	{
		//mengambil data produk berdasarakan id_produk
		$ambil_hasil = $this->koneksi->query("SELECT * FROM master_hasil_tani ORDER BY ID_Hasil DESC");
		$pecah_hasil = $ambil_hasil->fetch_assoc();
		$id_hasil = $pecah_hasil['ID_Hasil'];
		if($id_hasil=="")
		{
			$kode_hasil = "HAS0001";
		}
		else
		{
			$hasil = substr($id_hasil, 0, 7);
			$hasil++;
			$kode_hasil = sprintf("%04s", $hasil);
		}

		$this->koneksi->query("INSERT INTO master_hasil_tani (ID_Hasil, Nama_Hasil, ID_Spesies, Deskripsi_Hasil, Harga_Terendah, Harga_Tertinggi, Masa_Expayet, Satuan, ID_Kategori) VALUES ('$kode_hasil','$Nama_Hasil','$ID_Spesies','$Deskripsi_Hasil','$hargarendah','$hargatinggi','$masaexpayet','$Satuan','$ID_Kategori')") or die(mysqli_error($this->koneksi));



		$ambil = $this->koneksi->query("SELECT ID_Produk FROM master_produk_tani_commerce ORDER BY ID_Produk DESC");
		$pecah = $ambil->fetch_assoc();
		$kode = $pecah['ID_Produk'];
		if($kode=="")
		{
			$kode_produk="PRO0001";
		}
		else
		{
			$hasil = substr($kode, 0, 7);
			$hasil++;
			$kode_produk = sprintf("%04s", $hasil);
		}

		$this->koneksi->query("INSERT INTO master_produk_tani_commerce (ID, ID_Produk, ID_Kategori) VALUES ('$kode_hasil','$kode_produk','$ID_Kategori')");

		$tgl = date("Y/m/d");

		$user = $_SESSION["penjual"]["ID_User"];

		$this->koneksi->query("INSERT INTO trans_harga_prod (ID_Produk, ID_User, Tanggal, Harga) VALUES ('$kode_produk','$user','$tgl','$hargapas')");
		return 'berhasil';
	}

	//SIMPAN ALAT ADMIN PEJUAL
	function simpan_produk_alat($namalat, $deskripsisalat, $spesifikasialat, $hargaterendah, $hargatertinggi, $hargapas, $fungsi, $kategori_produk)
	{
		$ambil_alat = $this->koneksi->query("SELECT * FROM master_alat_tani ORDER BY ID_Alat DESC");
		$pecah_alat = $ambil_alat->fetch_assoc();
		$id_alat = $pecah_alat['ID_Alat'];
		if($id_alat=="")
		{
			$kode_alat = "ALA0001";
		}
		else
		{
			$hasil = substr($id_alat, 0, 7);
			$hasil++;
			$kode_alat = sprintf("%04s", $hasil);
		}
		$this->koneksi->query("INSERT INTO master_alat_tani (ID_alat, Nama_Alat, Deskripsi_Alat, Spesifikasi, Harga_Terendah, Harga_Tertinggi, Fungsi, ID_Kategori) VALUES ('$kode_alat','$namalat','$deskripsisalat','$spesifikasialat','$hargaterendah','$hargatertinggi','$fungsi','$kategori_produk') ");


		$ambil = $this->koneksi->query("SELECT ID_Produk FROM master_produk_tani_commerce ORDER BY ID_Produk DESC");
		$pecah = $ambil->fetch_assoc();
		$kode = $pecah['ID_Produk'];
		if($kode=="")
		{
			$kode_produk="PRO0001";
		}
		else
		{
			$hasil = substr($kode, 0, 7);
			$hasil++;
			$kode_produk = sprintf("%04s", $hasil);
		}

		$this->koneksi->query("INSERT INTO master_produk_tani_commerce (ID, ID_Produk, ID_Kategori) VALUES ('$kode_alat','$kode_produk','$kategori_produk')");

		$tgl = date("Y/m/d");

		$user = $_SESSION["penjual"]["ID_User"];

		$this->koneksi->query("INSERT INTO trans_harga_prod (ID_Produk, ID_User, Tanggal, Harga) VALUES ('$kode_produk','$user','$tgl','$hargapas')");
		return 'berhasil';
	}

	//SIMPAN BAHAN ADMIN PEJUAL
	function simpan_produk_bahan($namabahan, $deskripsibahan, $spesifikasibahan, $hargaterendah, $hargatertinggi, $hargapas, $fungsi, $jenisbahan, $kategori_produk)
	{
		$ambil_bahan = $this->koneksi->query("SELECT * FROM master_bahan_tani ORDER BY ID_Bahan DESC");
		$pecah_bahan = $ambil_bahan->fetch_assoc();
		$id_bahan = $pecah_bahan['ID_Bahan'];
		if($id_bahan=="")
		{
			$kode_bahan = "BAH0001";
		}
		else
		{
			$hasil = substr($id_bahan, 0, 7);
			$hasil++;
			$kode_bahan = sprintf("%04s", $hasil);
		}
		
		$this->koneksi->query("INSERT INTO master_bahan_tani (ID_Bahan, Nama_Bahan, Deskripsi_Bahan, Spesifikasi_Bahan, Harga_Terendah,  Harga_Tertinggi, Fungsi_Bahan, Jenis_Bahan, ID_Kategori) VALUES ('$kode_bahan','$namabahan','$deskripsibahan','$spesifikasibahan','$hargaterendah','$hargatertinggi','$fungsi', '$jenisbahan','$kategori_produk') ");

		$ambil = $this->koneksi->query("SELECT ID_Produk FROM master_produk_tani_commerce ORDER BY ID_Produk DESC");
		$pecah = $ambil->fetch_assoc();
		$kode = $pecah['ID_Produk'];
		if($kode=="")
		{
			$kode_produk="PRO0001";
		}
		else
		{
			$hasil = substr($kode, 0, 7);
			$hasil++;
			$kode_produk = sprintf("%04s", $hasil);
		}

		$this->koneksi->query("INSERT INTO master_produk_tani_commerce (ID, ID_Produk, ID_Kategori) VALUES ('$kode_bahan','$kode_produk','$kategori_produk')");

		$tgl = date("Y/m/d");

		$user = $_SESSION["penjual"]["ID_User"];

		$this->koneksi->query("INSERT INTO trans_harga_prod (ID_Produk, ID_User, Tanggal, Harga) VALUES ('$kode_produk','$user','$tgl','$hargapas')");
		return 'berhasil';
		
	}

	//SIMPAN PENAWARAN PRODUK PENJUAL
	function simpan_penawaran_produk($kode_produk, $spesifikasi, $kondisi, $merk, $thnpro, $gambar1, $gambar2, $stok, $satuan, $user)
	{
		//gambar1
		$namagambar1 = $gambar1["name"];
		$namafix = date("H_m_d")."_".$namagambar1;
		$lokasifoto = $gambar1["tmp_name"];
		$ekstensigambar1 = pathinfo($namafix,PATHINFO_EXTENSION);
		$boleh1 = array("jpg","png","jpeg","gif","JPG","JPEG","PNG","GIF");

		//gambar2
		$namagambar2 = $gambar2["name"];
		$namafix2 = date("H_m_d")."_".$namagambar2;
		$lokasifoto2 = $gambar2["tmp_name"];
		$ekstensigambar2 = pathinfo($namafix2,PATHINFO_EXTENSION);
		$boleh2 = array("jpg","png","jpeg","gif","JPG","JPEG","PNG","GIF");
		if(in_array($ekstensigambar1, $boleh1) AND in_array($ekstensigambar2, $boleh2))
		{
			$ambil = $this->koneksi->query("SELECT ID_Penawaran FROM trans_penawaran_prod_tani ORDER BY Nomor DESC");
			$pecah = $ambil->fetch_assoc();
			$kode = $pecah['ID_Penawaran'];
			if($kode=="")
			{
				$kode_penawaran="PEN0001";
			}
			else
			{
				$hasil = substr($kode, 0, 7);
				$hasil++;
				$kode_penawaran = sprintf("%04s", $hasil);
			}
			
			$tgl = date("Y/m/d");
			$status = "Tidak Aktif";

			move_uploaded_file($lokasifoto, "../asset/img/produk/$namafix");
			move_uploaded_file($lokasifoto2, "../asset/img/produk/$namafix2");
			$this->koneksi->query("INSERT INTO trans_penawaran_prod_tani (Spesifikasi_Barang, ID_Produk, Kondisi_Barang, Merk, Tahun_Produksi, Gambar1, Gambar2, Status, Stok, Satuan_Barang, ID_User, ID_Penawaran, Tanggal_Penawaran) VALUES ('$spesifikasi','$kode_produk','$kondisi','$merk','$thnpro','$namafix','$namafix2','$status','$stok','$satuan','$user','$kode_penawaran','$tgl')") or die(mysqli_error($this->koneksi)); 
			return "sukses";
		}
		else
		{
			return "gagal";
		} 
	}

	//HAPUS PRODUK ADMIN PEJUAL
	function hapus_hasil_tani($id, $idpro)
	{
		$this->koneksi->query("DELETE FROM master_hasil_tani WHERE ID_Hasil='$id'");
		$this->koneksi->query("DELETE FROM trans_harga_prod WHERE ID_Produk='$idpro'");
	}

	//HAPUS PRODUK ALAT ADMIN PEJUAL
	function hapus_produk_alat($id, $idpro)
	{
		$this->koneksi->query("DELETE FROM master_alat_tani WHERE ID_Alat='$id'");
		$this->koneksi->query("DELETE FROM trans_harga_prod WHERE ID_Produk='$idpro'");
	}

	//HAPUS PRODUK BAHAN ADMIN PEJUAL
	function hapus_produk_bahan($id, $idpro)
	{
		$this->koneksi->query("DELETE FROM master_bahan_tani WHERE ID_Bahan='$id'");
		$this->koneksi->query("DELETE FROM trans_harga_prod WHERE ID_Produk='$idpro'");
	}

	//AMBIL HASIL PRODUK ADMIN PEJUAL
	function ambil_hasil_produk($id, $idpro)
	{
		$ambil = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_hasil_tani ON master_produk_tani_commerce.ID=master_hasil_tani.ID_Hasil JOIN master_spesies_tanaman ON master_hasil_tani.ID_Spesies=master_spesies_tanaman.ID_Spesies WHERE ID_Hasil='$id' AND trans_harga_prod.ID_Produk='$idpro'");
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	//AMBIL PRODUK ALAT ADMIN PEJUAL
	function ambil_produk_alat($id, $idpro)
	{
		$ambil = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_alat_tani ON master_produk_tani_commerce.ID=master_alat_tani.ID_Alat WHERE ID_Alat='$id' AND trans_harga_prod.ID_Produk='$idpro'");
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	//AMBIL PRODUK BAHAN ADMIN PEJUAL
	function ambil_produk_bahan($id, $idpro)
	{
		$ambil = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_bahan_tani ON master_produk_tani_commerce.ID=master_bahan_tani.ID_Bahan WHERE ID_Bahan='$id' AND trans_harga_prod.ID_Produk='$idpro'");
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	//FUNGSI UNTUK MENAMPILKAN TABEL PENAWARAN DI ANTARMUKA PENJUAL
	function ambil_produk_penawaran($id)
	{
		$ambil1 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_hasil_tani ON master_produk_tani_commerce.ID=master_hasil_tani.ID_Hasil JOIN trans_penawaran_prod_tani ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE master_hasil_tani.ID_Hasil='$id'");
		$pecah1 = $ambil1->fetch_assoc();
		//menghitung jumlah data yang sesuai id_produk
		$cek1 = $ambil1->num_rows;
		if($cek1==1)
		{
			return $pecah1;
		}
		else
		{
			$ambil2 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_alat_tani ON master_produk_tani_commerce.ID=master_alat_tani.ID_Alat JOIN trans_penawaran_prod_tani ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE ID_Alat='$id'");
			$pecah2 = $ambil2->fetch_assoc();
			$cek2 = $ambil2->num_rows;
			if ($cek2==1)
			{
				return $pecah2;
			}
			else
			{
				$ambil3 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_bahan_tani ON master_produk_tani_commerce.ID=master_bahan_tani.ID_Bahan JOIN trans_penawaran_prod_tani ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE ID_Bahan='$id'");
				$pecah3 = $ambil3->fetch_assoc();

				$cek3 = $ambil3->num_rows;
				if ($cek3==1)
				{
					return $pecah3;
				}
				else
				{
					return 'gagal';
				}
			}
		}
	}

	function ambil_produk_ditawarkan($id)
	{
		$ambil1 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_hasil_tani ON master_produk_tani_commerce.ID=master_hasil_tani.ID_Hasil WHERE master_hasil_tani.ID_Hasil='$id'")or die(mysqli_error($this->koneksi));
		$pecah1 = $ambil1->fetch_assoc();
		//menghitung jumlah data yang sesuai id_produk
		
		$cek1 = $ambil1->num_rows;
		
		if($cek1==1)
		{
			return $pecah1;
		}
		else
		{
			
			$ambil2 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_alat_tani ON master_produk_tani_commerce.ID=master_alat_tani.ID_Alat WHERE master_alat_tani.ID_Alat='$id'");
			$pecah2 = $ambil2->fetch_assoc();
			$cek2 = $ambil2->num_rows;
			if ($cek2>0)
			{
				return $pecah2;
			}
			else
			{
				$ambil3 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_bahan_tani ON master_produk_tani_commerce.ID=master_bahan_tani.ID_Bahan WHERE master_bahan_tani.ID_Bahan='$id'");
				$pecah3 = $ambil3->fetch_assoc();

				$cek3 = $ambil3->num_rows;
				if ($cek3==1)
				{
					return $pecah3;
				}
				else
				{

					return 'gagal';
				}
			}
		}
	}

	//UBAH PRODUK ADMIN PEJUAL
	function ubah_hasil_produk($namahasil, $spesies, $deskripsi, $hargarendah, $hargatinggi, $masaexp, $hargapas, $satuan, $id_hasil, $idpro)
	{
		$this->koneksi->query("UPDATE master_hasil_tani SET Nama_Hasil='$namahasil', ID_Spesies='$spesies', Deskripsi_Hasil='$deskripsi', Harga_Terendah='$hargarendah', Harga_Tertinggi='$hargatinggi', Masa_Expayet='$masaexp', Satuan='$satuan' WHERE ID_Hasil='$id_hasil'") or die(mysqli_error($this->koneksi));

		$this->koneksi->query("UPDATE trans_harga_prod SET Harga='$hargapas' WHERE ID_Produk='$idpro'");
	}

	//UBAH PRODUK ALAT ADMIN PEJUAL
	function ubah_produk_alat($namaalat, $deskripsialat, $spesifikasi, $hargaterendah, $hargatertinggi, $hargapas, $fungsi, $idalat, $idpro)
	{
		$this->koneksi->query("UPDATE master_alat_tani SET Nama_Alat='$namaalat', Deskripsi_Alat='$deskripsialat', Spesifikasi='$spesifikasi', Harga_Terendah='$hargaterendah', Harga_Tertinggi='$hargatertinggi', Fungsi='$fungsi' WHERE ID_Alat='$idalat'");

		$this->koneksi->query("UPDATE trans_harga_prod SET Harga='$hargapas' WHERE ID_Produk='$idpro'");
	}

	//UBAH PRODUK BAHAN ADMIN PEJUAL
	function ubah_produk_bahan($namabahan, $deskripsibahan, $spesifikasibahan, $hargaterendah, $hargatertinggi, $hargapas, $fungsibahan, $jnsbahan, $idbahan, $idpro)
	{
		$this->koneksi->query("UPDATE master_bahan_tani SET Nama_Bahan='$namabahan', Deskripsi_Bahan='$deskripsibahan', Spesifikasi_Bahan='$spesifikasibahan', Harga_Terendah='$hargaterendah', Harga_Tertinggi='$hargatertinggi', Fungsi_Bahan='$fungsibahan', Jenis_Bahan='$jnsbahan' WHERE ID_Bahan='$idbahan'");

		$this->koneksi->query("UPDATE trans_harga_prod SET Harga='$hargapas' WHERE ID_Produk='$idpro'");
	}

	//MEJALANAKAN FUNGSI ADMIN UNTUK MELIHAT PRODUK BERDASARAKAN PENJUAL
	function tampil_detail_produk($id_user)
	{
		$semuaproduk = array();
		$ambil = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk WHERE trans_harga_prod.ID_User='$id_user'")or die(mysqli_error($this->koneksi));
		while($pecah = $ambil->fetch_assoc())
		{
			$semuaproduk[] = $pecah;
		}

		return $semuaproduk;
	}

	//MENJALANAKAN FUNGSI UBAH STATUS PRODUK VALIDASI PRODUK OLEH ADMIN
	function ubah_status_produk($id , $user)
	{
		$this->koneksi->query("UPDATE trans_penawaran_prod_tani SET Status='Aktif', Validasi_Admin='1' WHERE ID_Penawaran='$id'");

		// mengambil data user berdasarka id user
		$ambil = $this->koneksi->query("SELECT * FROM master_detail_user WHERE ID_User='$user'");
		$akun = $ambil->fetch_assoc();

		$pesan = "Selamat Produk sudah aktif";

		$tgl_jam = date('Y/m/d H:i:s');

		$this->koneksi->query("INSERT INTO pesan (nama, email, pesan, tanggal_jam, status) VALUES ('$akun[nama]','$akun[Email]','$pesan','$tgl_jam','unread')");
	}

	function cek_penawaran($id_produk)
	{
		$data = [];
		$ambil = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani WHERE ID_Produk='$id_produk'");
		while($pecah = $ambil->fetch_assoc())
		{
			$data[] = $pecah;
		}
		return $data;
	}

	function ambil_penawaran_produk($id_produk)
	{
		$ambil = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk WHERE trans_penawaran_prod_tani.ID_Produk='$id_produk'")or die(mysqli_error($this->koneksi));
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	function ambil_penawaran_produk_terlaris($id_produk)
	{
		$ambil = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk WHERE trans_penawaran_prod_tani.ID_Produk='$id_produk'")or die(mysqli_error($this->koneksi));
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	function ambil_penawaran($id)
	{
		$ambil = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk WHERE trans_penawaran_prod_tani.ID_Penawaran='$id'")or die(mysqli_error($this->koneksi));
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	function cari_harga_produk($Kategori, $nama_produk)
	{
		$ambil1 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_hasil_tani ON master_produk_tani_commerce.ID=master_hasil_tani.ID_Hasil JOIN trans_penawaran_prod_tani ON trans_harga_prod.ID_produk=trans_penawaran_prod_tani.ID_Produk WHERE Nama_Hasil LIKE '%$nama_produk%' AND master_hasil_tani.ID_Kategori = '$Kategori' ")or die(mysqli_error($this->koneksi));
		//menghitung jumlah data yang sesuai id_produk
		$cek1 = $ambil1->num_rows;
		if($cek1>0)
		{
			while($pecah1 = $ambil1->fetch_assoc())
			{
				$semua_data[] = $pecah1;
			}

			return $semua_data;
			
		}
		else
		{
			$ambil2 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_alat_tani ON master_produk_tani_commerce.ID=master_alat_tani.ID_Alat JOIN trans_penawaran_prod_tani ON trans_harga_prod.ID_produk=trans_penawaran_prod_tani.ID_Produk WHERE Nama_Alat LIKE '$nama_produk%' AND master_alat_tani.ID_Kategori =  '$Kategori'");
			
			$cek2 = $ambil2->num_rows;
			if ($cek2>0)
			{
				while($pecah2 = $ambil2->fetch_assoc())
				{
					$semua_data[] = $pecah2;
				}

				return $semua_data;
			}
			else
			{
				$ambil3 = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_bahan_tani ON master_produk_tani_commerce.ID=master_bahan_tani.ID_Bahan JOIN trans_penawaran_prod_tani ON trans_harga_prod.ID_produk=trans_penawaran_prod_tani.ID_Produk WHERE Nama_Bahan LIKE '$nama_produk%' AND master_bahan_tani.ID_Kategori = '$Kategori'");
				

				$cek3 = $ambil3->num_rows;
				if ($cek3>0)
				{
					while($pecah3 = $ambil3->fetch_assoc())
					{
						$semua_data[] = $pecah3;
					}

					return $semua_data;
				}
				else
				{
					return 'gagal';
				}
			}
		}
	}
}
$produk = new produk ($database);


class spesies
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi=$mysqli;
	}

	//TAMPIL SPESIES YANG MENJALANAKAN FUNGSI UNTUK TAMBAH DATA HASIL PRODUK DI ANTAR MUKA PENJUAL
	function tampil_spesies()
	{
		$semuaspesies=array();
		$ambilspesies = $this->koneksi->query("SELECT * FROM master_spesies_tanaman");
		while ($getspesies = $ambilspesies->fetch_assoc())
		{
			$semuaspesies[] = $getspesies;
		}
		return $semuaspesies;
	}
}
$spesies = new spesies($database);


class user
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi=$mysqli;
	}

	//TAMPIL DETAIL USER
	function tampil_detail_user()
	{
		$semuauser = array();
		$ambiluser = $this->koneksi->query("SELECT * FROM master_detail_user");
		while ($getuser = $ambiluser->fetch_assoc())
		{
			$semuauser[]= $getuser;
		}
		return $semuauser;
	}

	function ambil_detail_user($id)
	{
		$ambil = $this->koneksi->query("SELECT * FROM master_detail_user WHERE ID_User='$id'") or die(mysqli_error($this->koneksi));

		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}

	//SIMPAN USER(REGISTRASI)
	function simpan_user($pengguna, $namalengkap, $email, $katasandi, $pin)
	{

		$karakter ="1234567890";
		$id="";
		for($i=1; $i<=6; $i++)
		{
			$pos = rand(0, strlen($karakter)-1);
			$id.=$karakter[$pos];
		}

		$this->koneksi->query("INSERT INTO master_user (ID_User, Password, Pin) VALUES ('$id','$katasandi','$pin')") or die(mysqli_error($this->koneksi));
		
		$this->koneksi->query("INSERT INTO master_user_kat (ID_User, ID_Kategori) VALUES ('$id','$pengguna')") or die(mysqli_error($this->koneksi));

		$ambil = $this->koneksi->query("SELECT * FROM master_detail_user WHERE Email='$email'");

		$hasil = $ambil->num_rows;

		if ($hasil==1)
		{
			return 'gagal';
		}
		else
		{
			$this->koneksi->query("INSERT INTO master_detail_user (ID_User,nama, Email) VALUES ('$id','$namalengkap','$email')") or die(mysqli_error($this->koneksi));
			return 'berhasil';

			if($pengguna=="PJL")
			{
				$mail = "";
				$to = $email;
				$subject = "Verifikasi Email";

				$message = "Respon email ini, Tunggu Konfirmasi Admin";

		// 
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// 
				$headers .= 'From: <cs@dodolantani.com>' . "\r\n";

				mail($to,$subject,$message,$headers);
			}

			return $id;

		}

	}

	//SIMPAN USER(DETAIL REGISTRASI)
	function simpan_detail_user($id, $jenis_kelamin, $tgllhair, $alamat, $provinsi, $kabupaten, $kecamatan, $kelurahan, $notlpn, $foto)
	{

		$namafoto = $foto["name"];
		$fotofix = date("Y_m_d_H_m_d")."_".$namafoto;
		$lokasifoto = $foto["tmp_name"];
		$ekstensifoto = pathinfo($fotofix,PATHINFO_EXTENSION);
		$boleh = array("jpg","png","jpeg","gif","JPG","JPEG","PNG","GIF");

		if(in_array($ekstensifoto, $boleh))
		{
			move_uploaded_file($lokasifoto, "asset/img/foto/$fotofix");
			$this->koneksi->query("UPDATE master_detail_user SET jenis_kelamin='$jenis_kelamin', tanggal_lahir='$tgllhair', alamat='$alamat', provinsi='$provinsi', kabupaten='$kabupaten', kecamatan='$kecamatan', kelurahan_desa='$kelurahan', nomor_telpon='$notlpn', Foto='$fotofix', Status_Akun='Tidak Aktif' WHERE ID_User='$id'") or die(mysqli_error($this->koneksi));
			return "sukses";
		}
		else
		{
			return "gagal";
		}
	}

	function hapus_detail_user($id_user)
	{
		$this->koneksi->query("DELETE FROM master_user WHERE ID_User='$id_user'");
		$this->koneksi->query("DELETE FROM master_detail_user WHERE ID_User='$id_user'");
		$this->koneksi->query("DELETE FROM master_user_kat WHERE ID_User='$id_user'");
	}

	function ubah_detail_user($nama, $jns_kelamin, $tgl_lhr, $alamat, $provinsi, $kabupaten, $kecamatan, $kelurahan_desa, $nomor_telpon, $email, $id_user)
	{
		$this->koneksi->query("UPDATE master_detail_user SET nama='$nama', jenis_kelamin='$$jns_kelamin', tanggal_lahir='$tgl_lhr', alamat='$alamat', provinsi='$provinsi', kabupaten='$kabupaten', kecamatan='$kecamatan', kelurahan_desa='$kelurahan_desa', nomor_telpon='$nomor_telpon', Email='$email' WHERE ID_User='$id_user'") or die(mysqli_error($this->koneksi));
	}
}
$user = new user($database);

class penjual
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi= $mysqli;
	}

	//TAMPIL USER YANG TERDAFATAR SEBAGAI PENJUAL
	function tampil_penjual()
	{
		$semuapenjual = array();
		$ambilpenjual= $this->koneksi->query("SELECT * FROM master_user_kat JOIN master_detail_user ON master_user_kat.ID_User=master_detail_user.ID_User WHERE ID_Kategori='PJL'" );
		while ($getpenjual = $ambilpenjual->fetch_assoc())
		{
			$semuapenjual[] = $getpenjual;
		}
		return $semuapenjual;
	}

	//MENJALANKAN FUNGSI LOGIN UNTUK USER YANG TERDAFTAR SEBAGAI PENUAL
	function login_penjual($email, $pass)
	{
		$ps = SHA1 ($pass);
		//$ambil = $this->koneksi->query("SELECT * FROM master_detail_user JOIN master_user ON master_detail_user.ID_User=master_user.ID_User WHERE Email='$email' AND Password='$ps' AND Status_Akun='Aktif'");
		$ambil = $this->koneksi->query("SELECT * FROM master_detail_user JOIN master_user ON master_detail_user.ID_User=master_user.ID_User WHERE master_user.ID_User='$email' AND Password='$ps' AND Status_Akun='Aktif'");
		$cek = $ambil->num_rows;

		if($cek==1)
		{
			$akun = $ambil->fetch_assoc();
			$_SESSION['penjual'] = $akun;
			return "sukses";
		}
		else
		{
			return "gagal";
		}
	}

	//MENGUBAH STATUS PENJUAL MENJADI AKTIF(VALIDASI AKUN PENJUAL)
	function ubah_status_penjual($id)
	{
		$this->koneksi->query("UPDATE master_detail_user SET Status_Akun='Aktif' WHERE ID_User='$id'");

		$ambil = $this->koneksi->query("SELECT * FROM master_detail_user WHERE ID_User='$id'");

		$akun = $ambil->fetch_assoc();

		$pesan = "Hai, Selamat kamu sudah bisa melakukan penjualan Produk";

		$tgl_jam = date('Y/m/d H:i:s');

		$this->koneksi->query("INSERT INTO pesan (nama, email, pesan, tanggal_jam, status) VALUES ('$akun[nama]','$akun[Email]',' $pesan','$tgl_jam','unread')");
		$mail = $akun['Email'];
		$to = $akun['Email'];
		$subject = "Verfikasi Email";

		$message = "Akun anda telah aktif";

		// 
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// 
		$headers .= 'From: <cs@dodolantani.com>' . "\r\n";

		mail($to,$subject,$message,$headers);
	}

	function tampil_diskusi_produk_penjual($id_user)
	{
		$semuadiskusi = array();
		$ambildiskusi= $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_topik_diskusi ON trans_penawaran_prod_tani.ID_Produk=trans_topik_diskusi.ID_Produk JOIN master_detail_user ON master_detail_user.ID_User=trans_penawaran_prod_tani.ID_User WHERE trans_penawaran_prod_tani.ID_User='$id_user'");
		while ($getdiskusi = $ambildiskusi->fetch_assoc())
		{
			$semuadiskusi[] = $getdiskusi;
		}
		return $semuadiskusi;
	}

	function tampil_diskusi($id)
	{
		$semuaulasan = array();
		$ambilisi = $this->koneksi->query("SELECT * FROM trans_topik_diskusi JOIN master_detail_user ON trans_topik_diskusi.ID_User=master_detail_user.ID_User JOIN trans_penawaran_prod_tani ON trans_penawaran_prod_tani.ID_Produk=trans_topik_diskusi.ID_Produk JOIN master_user_kat ON master_user_kat.ID_User=master_detail_user.ID_User  JOIN trans_harga_prod ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk WHERE trans_topik_diskusi.ID_Produk='$id' AND parent='0'") or die(mysqli_error($this->koneksi));
		while($getisi = $ambilisi->fetch_assoc())
		{
			$semuaulasan[] = $getisi;
		}
		return $semuaulasan;
	}

	function balas_diskusi($isi, $tgl, $waktu, $id, $user, $kategori, $judul, $produk)
	{
		$this->koneksi->query("INSERT INTO trans_topik_diskusi (isi_diskusi,tanggal,waktu, parent, ID_user, ID_kategori_topik, Judul_Topik, ID_Produk) VALUES ('$isi','$tgl','$waktu','$id','$user','$kategori','$judul','$produk')")or die(mysqli_error($this->koneksi));
	}

	function tampil_ulasan($id_produk)
	{
		$semuaulasan = array();
		$ambilulasan = $this->koneksi->query("SELECT * FROM trans_komentar_diskusi JOIN trans_penawaran_prod_tani ON trans_penawaran_prod_tani.ID_Produk=trans_komentar_diskusi.ID_Produk JOIN trans_harga_prod ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk JOIN master_detail_user ON master_detail_user.ID_User=trans_komentar_diskusi.ID_user WHERE trans_komentar_diskusi.ID_Produk='$id_produk'");
		while ($getulasan = $ambilulasan->fetch_assoc())
		{
			$semuaulasan[] = $getulasan;
		}
		return $semuaulasan;
	}

	function balas($id,$parent)
	{
		$semuaulasan = array();
		$ambilisi = $this->koneksi->query("SELECT * FROM trans_topik_diskusi JOIN master_detail_user ON trans_topik_diskusi.ID_User=master_detail_user.ID_User JOIN trans_penawaran_prod_tani ON trans_penawaran_prod_tani.ID_Produk=trans_topik_diskusi.ID_Produk JOIN master_user_kat ON master_user_kat.ID_User=master_detail_user.ID_User  JOIN trans_harga_prod ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk WHERE trans_topik_diskusi.ID_Produk='$id' AND parent='$parent'") or die(mysqli_error($this->koneksi));
		while($getisi = $ambilisi->fetch_assoc())
		{
			$semuaulasan[] = $getisi;
		}
		return $semuaulasan;	
	}

	function laporan_penjualan_perkategori($id_kat, $bulan, $tahun, $id)
	{
		$semualaporan = array();
		$ambillaporan = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_penawaran_prod_tani.ID_Penawaran=trans_permintaan.ID_Penawaran JOIN trans_harga_prod ON trans_harga_prod.ID_Produk=trans_penawaran_prod_tani.ID_Produk JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_harga_prod.ID_Produk JOIN master_kategori_produk ON master_kategori_produk.ID_Kategori=master_produk_tani_commerce.ID_Kategori WHERE master_produk_tani_commerce.ID_Kategori='$id_kat' AND MONTH(Tgl_Permintaan)='$bulan' AND YEAR(Tgl_Permintaan)='$tahun' AND trans_penawaran_prod_tani.ID_User='$id'")or die(mysqli_error($this->koneksi));
		while($getlaporan = $ambillaporan->fetch_assoc())
		{
			$semualaporan[] = $getlaporan;
		}
		return $semualaporan;
	}

	function pesan_belum_dibaca()
	{
		$semuapesan = array();
		$ambilpesan = $this->koneksi->query("SELECT * FROM trans_topik_diskusi WHERE Status_Diskusi='Belum di baca'");
		while($getpesan = $ambilpesan->fetch_assoc())
		{
			$semuapesan[] = $getpesan;
		}
		return $semuapesan;
	}
}
$penjual = new penjual($database);


class admin
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi = $mysqli;
	}

	//MENJALANAKAN FUNGSI LOGIN UNTUK ADMIN
	function login_admin($email,$pass)
	{
		$ps = SHA1($pass);
		$ambil = $this->koneksi->query("SELECT * FROM master_user_org WHERE Email='$email' AND Password='$ps'");
		$cek = $ambil->num_rows;

		if($cek==1)
		{
			$akun = $ambil->fetch_assoc();
			$_SESSION['admin'] = $akun;
			return "sukses";
		}
		else
		{
			return "gagal";
		}
	}

	//TAMPIL PRODUK ADMIN
	function tampil_hasil_produk()
	{
		$semuahasil = array();
		$ambilhasil = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_hasil_tani ON master_produk_tani_commerce.ID=master_hasil_tani.ID_Hasil JOIN master_spesies_tanaman ON master_hasil_tani.ID_Spesies=master_spesies_tanaman.ID_Spesies");
		while ($gethasil = $ambilhasil->fetch_assoc())
		{
			$semuahasil[] = $gethasil;
		}
		return $semuahasil;
	}

	//TAMPIL ALAT TANI ADMIN
	function tampil_produk_alat()
	{
		$semuaalat = array();
		$ambilalat= $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_alat_tani ON master_produk_tani_commerce.ID=master_alat_tani.ID_Alat");
		while ($getalat = $ambilalat->fetch_assoc())
		{
			$semuaalat[] = $getalat;
		}
		return $semuaalat;
	}

	//TAMPIL BAHAN TANI ADMIN
	function tampil_produk_bahan()
	{
		$semuabahan = array();
		$ambilbahan = $this->koneksi->query("SELECT * FROM trans_harga_prod JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk JOIN master_bahan_tani ON master_produk_tani_commerce.ID=master_bahan_tani.ID_Bahan");
		while ($getbahan = $ambilbahan->fetch_assoc())
		{
			$semuabahan[] = $getbahan;
		}
		return $semuabahan;
	}

	function tampil_penawaran_produk_tidak_aktif()
	{
		$semuadata = array();
		$ambildata = $this->koneksi->query("SELECT * FROM trans_penawaran_prod_tani WHERE Status_Produk='Tidak Aktif'");
		while ($getdata = $ambildata->fetch_assoc())
		{
			$semuadata[] = $getdata;
		}
		return $semuadata;
	}

	function tampil_penjual_tidak_aktif()
	{
		$semuastatus = array();
		$ambilstatus = $this->koneksi->query("SELECT * FROM master_detail_user WHERE Status_Akun='Tidak Aktif'");
		while($getstatus = $ambilstatus->fetch_assoc())
		{
			$semuastatus[] = $getstatus;
		}
		return $semuastatus;
	}

	function kirim_bukti_penjual($nama, $no_rek, $jml_transfer, $bukti, $keterangan, $id_permintaan, $id_user)
	{
		$namabukti = $bukti["name"];
		$buktifix = date("Y_m_d_H_m_d")."_".$namabukti;
		$lokasibukti = $bukti["tmp_name"];
		$ekstensibukti = pathinfo($buktifix,PATHINFO_EXTENSION);
		$boleh = array("jpg","png","jpeg","gif","JPG","JPEG","PNG","GIF");

		if(in_array($ekstensibukti, $boleh))
		{
			move_uploaded_file($lokasibukti, "../asset/img/bukti/$buktifix");
			$this->koneksi->query("INSERT INTO trans_bukti_pembayaran (ID_Permintaan, ID_User, Nama_Rekening, No_Rekening, Jumlah_Bayar, Bukti, Keterangan) VALUES ('$id_permintaan','$id_user','$nama','$no_rek','$jml_transfer','$buktifix','$keterangan')") or die(mysqli_error($this->koneksi));
			return "sukses";
		}
		else
		{
			return "gagal";
		}
	}

	function ambil_bukti_transfer($id)
	{
		$ambilbukti = $this->koneksi->query("SELECT * FROM trans_bukti_pembayaran WHERE ID_Permintaan='$id'");
		$getbukti = $ambilbukti->fetch_assoc();

		return $getbukti;
	}

	function ambil_bukti_transfer_admin($id, $user)
	{
		$ambilbukti = $this->koneksi->query("SELECT * FROM trans_bukti_pembayaran WHERE ID_Permintaan='$id' AND ID_User='$user'");
		$getbukti = $ambilbukti->fetch_assoc();

		return $getbukti;
	}

}
$admin = new admin($database);

class pembeli extends produk
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi= $mysqli;
	}

	function tampil_transaksi_beli($ID_User)
	{
		$semuabeli = array();
		$ambilbeli = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_user_kat ON master_user_kat.ID_User=trans_permintaan.ID_User WHERE trans_penawaran_prod_tani.ID_User='$ID_User' AND ID_Kategori='PMB'")or die(mysqli_error($this->koneksi));
		while($getbeli = $ambilbeli->fetch_assoc())
		{
			$semuabeli[] = $getbeli;
		}
		return $semuabeli;
	}

	function tampil_semua_transaksi()
	{
		$semuabeli = array();
		$ambilbeli = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran")or die(mysqli_error($this->koneksi));
		while($getbeli = $ambilbeli->fetch_assoc())
		{
			$semuabeli[] = $getbeli;
		}
		return $semuabeli;
	}

	function cari_transaksi($tgl1, $tgl2, $ID_User)
	{
		$semuatrans = array();
		$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran WHERE trans_penawaran_prod_tani.ID_User='$ID_User' AND Tgl_Permintaan BETWEEN '$tgl1' AND '$tgl2'");
		while($gettrans = $ambiltrans->fetch_assoc())
		{
			$semuatrans[] = $gettrans;
		}
		return $semuatrans;
	}

	function cari_transaksi_penjual($tgl1, $tgl2, $kategori, $penjual)
	{
		// jika tgl1 dan tgl 2 kosong
		if(empty($tgl1) AND empty($tgl2) AND empty($kategori))
		{
			$semuatrans = array();
			$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE trans_penawaran_prod_tani.ID_User='$penjual' ") or die(mysqli_erro($this->koneksi));

			while($gettrans = $ambiltrans->fetch_assoc())
			{
				$semuatrans[] = $gettrans;
			}
			return $semuatrans;
		}
		elseif(empty($tgl1) AND empty($tgl2) AND empty($penjual))
		{
			$semuatrans = array();
			$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE master_produk_tani_commerce.ID_Kategori='$kategori' ")or die(mysqli_erro($this->koneksi));

			while($gettrans = $ambiltrans->fetch_assoc())
			{
				$semuatrans[] = $gettrans;
			}
			return $semuatrans;	
		}
		elseif(empty($tgl1) AND empty($tgl2))
		{
			$semuatrans = array();
			$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE trans_penawaran_prod_tani.ID_User='$penjual' AND master_produk_tani_commerce.ID_Kategori='$kategori' ")or die(mysqli_erro($this->koneksi));

			while($gettrans = $ambiltrans->fetch_assoc())
			{
				$semuatrans[] = $gettrans;
			}
			return $semuatrans;	
		}
		elseif(empty($penjual))
		{
			$semuatrans = array();
			$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE Tgl_Permintaan BETWEEN '$tgl1' AND '$tgl2' AND master_produk_tani_commerce.ID_Kategori='$kategori'")or die(mysqli_erro($this->koneksi));

			while($gettrans = $ambiltrans->fetch_assoc())
			{
				$semuatrans[] = $gettrans;
			}
			return $semuatrans;	
		}
		elseif(empty($kategori))
		{
			$semuatrans = array();
			$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE Tgl_Permintaan BETWEEN '$tgl1' AND '$tgl2' AND trans_penawaran_prod_tani.ID_User='$penjual'")or die(mysqli_erro($this->koneksi));

			while($gettrans = $ambiltrans->fetch_assoc())
			{
				$semuatrans[] = $gettrans;
			}
			return $semuatrans;	
		}
		else
		{
			$semuatrans = array();
			$ambiltrans = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_produk_tani_commerce ON master_produk_tani_commerce.ID_Produk=trans_penawaran_prod_tani.ID_Produk WHERE Tgl_Permintaan BETWEEN '$tgl1' AND '$tgl2' AND trans_penawaran_prod_tani.ID_User='$penjual' AND master_produk_tani_commerce.ID_Kategori='$kategori'") or die(mysqli_erro($this->koneksi));

			while($gettrans = $ambiltrans->fetch_assoc())
			{
				$semuatrans[] = $gettrans;
			}
			return $semuatrans;
			
		}
	}


	function tampil_pembayaran($kode)
	{
		$semuabayar = array();
		$ambilbayar = $this->koneksi->query("SELECT * FROM trans_pembayaran WHERE ID_Permintaan='$kode'");
		$getbayar = $ambilbayar->fetch_assoc();
		return $getbayar;
	}

	function ambil_permintaan($id)
	{
		$semuapermintaan = array();
		$ambilpermintaan = $this->koneksi->query("SELECT * FROM trans_permintaan WHERE ID_Permintaan='$id'");
		$getpermintaan = $ambilpermintaan->fetch_assoc();
		return $getpermintaan;
	}

	function ubah_status_permintaan($id , $user)
	{
		$this->koneksi->query("UPDATE trans_permintaan SET Status='Sudah Bayar' WHERE ID_Permintaan='$id' AND ID_User='$user'")or die(mysqli_erro($this->koneksi));

		// mengambil data user berdasarkan id user
		$ambil = $this->koneksi->query("SELECT * FROM master_detail_user WHERE ID_User='$user'")or die(mysqli_erro($this->koneksi));
		$akun = $ambil->fetch_assoc();

		$pesan = "Selamat Transaksi Sudah Bayar";

		$tgl_jam = date('Y/m/d H:i:s');

		$this->koneksi->query("INSERT INTO pesan (nama, email, pesan, tanggal_jam, status) VALUES ('$akun[nama]','$akun[Email]','$pesan','$tgl_jam','unread')")or die(mysqli_error($this->koneksi));
	}

	function ubah_status_kirim($id)
	{
		$this->koneksi->query("UPDATE trans_permintaan SET Status_Kirim='Sudah Kirim' WHERE ID_Permintaan='$id' ");
	}

	function cari_status_pengirman($kurir, $noresi)
	{
		$ambildata = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN master_detail_user ON master_detail_user.ID_User=trans_permintaan.ID_User JOIN master_user_kat ON master_user_kat.ID_User=master_detail_user.ID_User WHERE Nama_Ekspedisi='$kurir' AND No_Resi='$noresi' AND ID_Kategori='PMB'") or die(mysqli_error($this->koneksi));
		$cekdata = $ambildata->fetch_assoc();

		return $cekdata;
	}

	function cari_berdasarakan_sorting($cari, $id_user)
	{
		if ($cari=="Sudah Kirim" OR $cari=="Belum di Kirim")
		{
			$semuacari = array();
			$data_cari = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran WHERE trans_penawaran_prod_tani.ID_User='$id_user' AND Status_Kirim='$cari'");
			while($getcari = $data_cari->fetch_assoc())
			{
				$semuacari[] = $getcari;
			}
			return $semuacari;
		}
		else
		{
			$semuacari = array();
			$data_cari = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran WHERE trans_penawaran_prod_tani.ID_User='$id_user' AND Status='$cari'");
			while($getcari = $data_cari->fetch_assoc())
			{
				$semuacari[] = $getcari;
			}
			return $semuacari;
		}
	}

	function tampil_transaksi_beli_penjual()
	{
		$semuabeli = array();
		$ambilbeli = $this->koneksi->query("SELECT * FROM trans_permintaan JOIN trans_penawaran_prod_tani ON trans_permintaan.ID_Penawaran=trans_penawaran_prod_tani.ID_Penawaran JOIN master_user_kat ON master_user_kat.ID_User=trans_permintaan.ID_User WHERE ID_Kategori='PMB'")or die(mysqli_error($this->koneksi));
		while($getbeli = $ambilbeli->fetch_assoc())
		{
			$semuabeli[] = $getbeli;
		}
		return $semuabeli;
	}
}

$pembeli = new pembeli($database);

?>