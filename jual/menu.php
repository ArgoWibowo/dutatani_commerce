<nav class="navbar navbar-default">
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="index.php">Dodolantani</a></li>
			<li><a href="index.php?halaman=home">Produk</a></li>
			<li><a href="index.php?halaman=info">Infomasi</a></li>
		</ul>

		<ul class="nav navbar-nav navbar-right">
			<?php if (!isset($_SESSION["pelanggan"])): ?>
			<li><a href="login.php">Login</a></li>
			<li><a href="index.php?halaman=register">Registrasi</a></li>
			<?php endif ?>
		</ul>
	</div>
</nav>