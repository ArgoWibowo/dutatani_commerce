<title>Register</title>
<div class="container">
	<h2>Daftar di Dodolantani</h2>
	<br>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group">
			<select name="pengguna" class="form-control">
				<option>--Pilih Jenis Pengguna--</option>
				<option value="PMB">Pembeli</option>
				<option value="PJL">Penjual</option>
			</select>
		</div>
		<div class="form-group">
			<label>Username</label>
			<input type="text" class="form-control" name="username">
		</div>
		<div class="form-group">
			<label>Nama Lengkap</label>
			<input type="text" class="form-control" name="nama_lengkap">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="email" class="form-control" name="email">
		</div>
		<div class="form-group">
			<label>Kata Sandi</label>
			<input type="password" class="form-control" name="kata_sandi">
		</div>
		<div class="form-group">
			<label>Pin</label>
			<input type="number" class="form-control" name="pin">
		</div>
		<center><button class="btn btn-success" name="lanjut">Lanjutkan</button></center>
		<br>
		<br>
	</form>
	<?php 
	if(isset($_POST['lanjut']))
	{
		$id = $user->simpan_user($_POST['pengguna'], $_POST['username'], $_POST['nama_lengkap'], $_POST['email'], sha1($_POST['kata_sandi']), $_POST['pin']);

		if($id=='berhasil')
		{

			echo "<meta http-equiv='refresh' content='1;url=index.php?halaman=data_lengkap&id=$id'>";
		}
		else
		{
			echo "<script>alert('Email sudah terdaftar');</script>";
			echo "<script>location='index.php?halaman=register';</script>";
		}
	}

	?>

</div>

