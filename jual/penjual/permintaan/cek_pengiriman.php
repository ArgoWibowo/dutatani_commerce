<h2>Cek Pengiriman</h2>
<br>
<form method="post" class="form-inline">
	<select class="form-control" name="kurir">
		<option>-- Pilih Kurir --</option>
		<option value="JNE">JNE</option>
		<option value="POS">POS</option>
		<option value="TIKI">TIKI</option>
	</select>
	<input type="text" name="no_resi" class="form-control" placeholder="Masukan Nomor Resi">
	<button class="btn btn-success" name="cari">Cari</button>
</form>

<?php
$beli['Nama_Hasil']="";
$beli['Nama_Alat']="";
$beli['Nama_Bahan']="";
$beli['Deskripsi_Hasil']='';
$beli['Deskripsi_ALat']='';
$beli['Deskripsi_Bahan']='';
if(isset($_POST["cari"]))
{
	$datacari = $pembeli->cari_status_pengirman($_POST['kurir'], $_POST['no_resi']);
	if (empty($datacari)) {
		echo "<script>alert('Data Tidak ditemukan');</script>";
	}
	else
	{
		echo "<script>alert('Data ditemukan');</script>";
	}
	


	$detail_beli = $produk->ambil_penawaran($datacari['ID_Penawaran']);
	$beli = $produk->ambil_produk_penawaran($detail_beli['ID']);
}



?>
<br>
<?php if (!empty($datacari)): ?>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>DETAIL BELI</th>
				<th>KODE PEMBELI</th>
				<th>NO INVOICE</th>
				<th>QTY</th>
				<th>HARGA</th>
				<th>TANGGAL KEBUTUHAN</th>
				<th>TANGGAL PERMINTAAN</th>
				<th>STATUS BAYAR</th>
				<th>STATUS KIRIM</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
				<a data-toggle="modal" data-target="#myModal" class="label label-danger"><?php echo $datacari['ID_Penawaran']; ?></a></td>
				<td> <?php echo $datacari['ID_User']; ?></td>
				<td> <?php echo $datacari['ID_Permintaan']; ?></td>
				<td> <?php echo $datacari['Qty']; ?></td>
				<td> <?php echo $datacari['Harga']; ?></td>
				<td> <?php echo $datacari['Tgl_Kebutuhan']; ?></td>
				<td> <?php echo $datacari['Tgl_Permintaan']; ?></td>
				<td> <?php echo $datacari['Status']; ?></td>
				<td> <?php echo $datacari['Status_Kirim']; ?></td>
			</tr>
		</tbody>
	</table>
<?php endif ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<center><img src="../asset/img/produk/<?php echo $beli['Gambar1']; ?>" class="img-responsive"></center>
				<br>
				<table class="table">
					<tr>
						<th>Nama Produk</th>
						<td>
							<?php
							if(!empty($beli['Nama_Hasil']))
							{
								echo $beli['Nama_Hasil'];
							}
							elseif(!empty($beli['Nama_Alat']))
							{
								echo $beli['Nama_Alat'];	
							}
							elseif(!empty($beli['Nama_Bahan']))
							{
								echo $beli['Nama_Bahan'];
							}
							?>
						</td>
					</tr>
					<tr>
						<th>Harga Produk</th>
						<td> <?php echo $detail_beli['Harga']; ?></td>
					</tr>
					<!-- <tr>
						<th>Masa Exp</th>
						<td> <?php echo $detail_beli['Masa_Expayet']; ?></td>
					</tr> -->
					<tr>
						<th>Satuan </th>
						<td> <?php echo $detail_beli['Satuan_Barang']; ?></td>
					</tr>
					<tr>
						<th>Spesifikasi Barang </th>
						<td> <?php echo $detail_beli['Spesifikasi_Barang']; ?></td>
					</tr>
					<tr>
						<th>Deskripsi</th>
						<td> 
						<?php
							if(!empty($beli['Deskripsi_Hasil']))
							{
								echo $beli['Deskripsi_Hasil'];
							}
							elseif(!empty($beli['Deskripsi_ALat']))
							{
								echo $beli['Deskripsi_ALat'];	
							}
							elseif(!empty($beli['Deskripsi_Bahan']))
							{
								echo $beli['Deskripsi_Bahan'];
							}
							?>
						</td>
					</tr>
					<tr>
						<th>Kondisi Barang </th>
						<td> <?php echo $detail_beli['Kondisi_Barang']; ?></td>
					</tr>
					<tr>
						<th>Merk </th>
						<td> <?php echo $detail_beli['Merk']; ?></td>
					</tr>
					<tr>
						<th>Tahun Produksi </th>
						<td> <?php echo $detail_beli['Tahun_Produksi']; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- <pre>
	<?php print_r($data); ?>
</pre>   -->