<?php

$iduser = $_SESSION['penjual']['ID_User'];

$data_bukti = $admin->ambil_bukti_transfer_admin($_GET['id'], $iduser);
?>

<h3>Bukti Transfer dari Admin</h3>

<?php if (empty($data_bukti)): ?>
	<div class="alert alert-danger">
		Belum ada bukti transfer dari Admin
	</div>
<?php else: ?>
	<div class="row">
		<div class="col-md-6"> 
			<img src="../asset/img/bukti/<?php echo $data_bukti['Bukti']; ?>" class="img-square img-responsive" data-toggle="modal" data-target="#myModal"></div>
			<div class="col-md-6">
				<table class="table">
					<tr>
						<th>KODE PERMINTAAN</th>
						<td> <?php echo $data_bukti['ID_Permintaan']; ?></td>
					</tr>
					<tr>
						<th>KODE USER</th>
						<td> <?php echo $data_bukti['ID_User']; ?></td>
					</tr>
					<tr>
						<th>NAMA PEMILIK</th>
						<td> <?php echo $data_bukti['Nama_Rekening']; ?></td>
					</tr>
					<tr>
						<th>NO REKENING</th>
						<td> <?php echo $data_bukti['No_Rekening']; ?></td>
					</tr>
					<tr>
						<th>JUMLAH TRANSFER</th>
						<td> <?php echo $data_bukti['Jumlah_Bayar']; ?></td>
					</tr>
					<tr>
						<th>KETERANGAN</th>
						<td> <?php echo $data_bukti['Keterangan']; ?></td>
					</tr>
				</table>
			</div>
		</div>
	<?php endif ?>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<center><img src="../asset/img/bukti/<?php echo $data_bukti['Bukti_Pembayaran']; ?>" class="img-responsive"></center>
				</div>
			</div>
		</div>
	</div>