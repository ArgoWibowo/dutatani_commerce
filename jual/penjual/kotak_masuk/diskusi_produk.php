<?php 
$dataproduk['Nama_Hasil']="";
$dataproduk['Nama_Alat']="";
$dataproduk['Nama_Bahan']="";

$id_user = $_SESSION['penjual']['ID_User'];

$datadiskusi = $penjual->tampil_diskusi_produk_penjual($id_user);

$dataproduk = $produk->tampil_penawaran_produk_user($id_user);

?>

<h3>Diskusi Produk</h3>
<br>
<div class="row">
	<?php foreach ($dataproduk as $key => $value): ?>
		<?php $data = $produk->ambil_penawaran_produk($value['ID_Produk']); ?>
		<?php $data_produk = $produk->ambil_produk_penawaran($data['ID']); ?>
		<div class="col-md-4">
			<div class="thumbanil">
				<center><img src="../asset/img/produk/<?php echo $value['Gambar1']; ?>" class="img-responsive" width="150"></center><br>
				<div class="caption">
				<center><?php
					if(!empty($data_produk['Nama_Hasil']))
					{
						echo $data_produk['Nama_Hasil'];
					}
					elseif(!empty($data_produk['Nama_Alat']))
					{
						echo $data_produk['Nama_Alat'];	
					}
					elseif(!empty($data_produk['Nama_Bahan']))
					{
						echo $data_produk['Nama_Bahan'];
					}
					?></center>
					<br>
					<br>
					<center><a href="index.php?halaman=isi&id_produk=<?php echo $data['ID_Produk']; ?>&id_user=<?php echo $data['ID_User']; ?>" class="btn btn-success">Diskusi Produk</a></center>
					<br>
					<br>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>
