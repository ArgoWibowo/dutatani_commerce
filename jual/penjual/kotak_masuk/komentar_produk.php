<?php 
$user = $_SESSION['penjual'];
$id_user = $_SESSION['penjual']['ID_User'];

$dataproduk = $produk->tampil_penawaran_produk_user($id_user);

$dataproduk['Nama_Hasil']="";
$dataproduk['Nama_Alat']="";
$dataproduk['Nama_Bahan']="";
$dataproduk['Deskripsi_Hasil']='';
$dataproduk['Deskripsi_ALat']='';
$dataproduk['Deskripsi_Bahan']='';

?>
<?php if ($dataproduk==array()): ?>
	<div class="alert alert-danger">Tidak Ada Diskusi</div>
<?php else: ?>
	<div class="row">
		<div class="col-md-8">
			<div>
				<table >
					<thead>
						<tr>
							<th> <?php echo $dataproduk[0]['ID_Produk']; ?> </th>
						</tr>
					</thead>
				</table>
				<br>
			</div>
			<?php foreach ($datadiskusi as $key => $value): ?>
				<div class="panel-default panel">
					<strong>Pembeli : </strong><strong class="label label-warning"><?php echo $value['nama']; ?></strong> <strong class="label label-success"><?php echo $value['waktu']; ?></strong>
					<br>
					<p><?php echo $value['isi_diskusi']; ?></p>
				</div>
				<?php if ($value['parent']==0): ?>
					<?php $balas = $penjual->balas($_GET['id_produk'], $value['ID_user']); ?>
					<?php foreach ($balas as $key => $isi): ?>
						<div class="panel-default panel">
							<strong>Penjual : </strong><strong class="label label-danger"><?php echo $isi['nama']; ?></strong> &nbsp;<strong class="label label-success"> <?php echo $isi['waktu'];?></strong>
							<br>
							<br>
							<p><?php echo $isi['isi_diskusi']; ?></p>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			<?php endforeach ?>
			<form method="post">
				<div class="form-group">
					<textarea class="form-control" placeholder="Balas Komentar Disini" name="isi_balasan"></textarea>
				</div>
				<div>
					<button class="btn btn-success" name="balas">Balas</button>
				</div>
			</form>
		</div>
	</div>
	<?php endif ?>

	<?php

	if (isset($_POST['balas']))
	{
		$tgl = date("Y/m/d");
		$waktu = date("Y/m/d H:i:s");
		$penjual->balas_diskusi($_POST['isi_balasan'], $tgl, $waktu, $parent['0'], $id_user, $kategori_topik['0'], $Judul_Topik[0], $_GET['id_produk']);

		echo "<meta http-equiv='refresh' content='1;url=index.php?halaman=isi&id_produk=$_GET[id_produk]&id_user=$_GET[id_user]'>";
	}

	?>
