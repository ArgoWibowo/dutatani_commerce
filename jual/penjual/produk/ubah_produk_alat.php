<?php 

$id_alat = $_GET['kode'];

$id = $_GET['id'];

$ambil = $produk->ambil_produk_alat($id_alat, $id);

?>

<h2>Ubah Produk</h2>
<form method="post" enctype="multipart/form-data">
	<br>
	<!-- <div class="form-group">
		<label>Kode Alat</label>
		<input type="text"  class="form-control" name="id_alat" value=" <?php echo $ambil['ID_Alat']; ?>" readonly>
	</div> -->
	<div class="form-group">
		<label>Nama Alat</label>
		<input type="text"  class="form-control" name="nama_alat" value=" <?php echo $ambil['Nama_Alat']; ?>">
	</div>
	<div class="form-group">
		<label>Deskripsi Alat</label>
		<textarea class="form-control" name="deskripsi_alat" required=""> <?php echo $ambil['Deskripsi_Alat']; ?></textarea>
	</div>
	<div class="form-group">
		<label>Spesifikasi</label>
		<textarea class="form-control" name="spesifikasi" required=""> <?php echo $ambil['Spesifikasi']; ?></textarea>
	</div>
	<div class="form-group">
		<label>Harga Terendah</label>
		<input type="number"  class="form-control" name="harga_terendah" value="<?php echo $ambil['Harga_Terendah']; ?>">
	</div>
	<div class="form-group">
		<label>Harga Tertinggi</label>
		<input type="number"  class="form-control" name="harga_tertinggi" value="<?php echo $ambil['Harga_Tertinggi']; ?>">
	</div>
	<div>
		<label>Harga Jual</label>
		<input type="number" class="form-control" name="Harga_Pas" required="" value="<?php echo $ambil['Harga']; ?>">
	</div>
	<br>
	<div class="form-group">
		<label>Fungsi</label>
		<textarea class="form-control" name="fungsi" required=""> <?php echo $ambil['Fungsi']; ?></textarea>
	</div>
	<button class="btn btn-warning" name="simpan_alat">Simpan</button>
</form>

<?php 
if(isset($_POST["simpan_alat"]))
{
	$produk->ubah_produk_alat($_POST['nama_alat'], $_POST['deskripsi_alat'],$_POST['spesifikasi'],$_POST['harga_terendah'],$_POST['harga_tertinggi'], $_POST['Harga_Pas'], $_POST['fungsi'], $id_alat, $id);

	echo "<script>alert('Data berhasil diubah'); location='index.php?halaman=produk'</script>";
}

?>


