<?php $data_kategori = $kategori->tampil_kategori(); ?>

<h4>Masukan Nama Produk</h4>
<form method="post" enctype="multipart/form-data">
	<div class="form-group">
	<select name="kategori" class="form-control">
		<option value="">--Pilih Kategori--</option>
		<?php foreach ($data_kategori as $key => $value): ?>
			<option value="<?php echo $value['ID_Kategori']; ?>"> <?php echo $value['Nama_kategori']; ?></option>
		<?php endforeach ?>
	</select>
	</div>
	<div class="form-group">
		<input type="text"  class="form-control" name="nama_produk" required="" placeholder="Nama Produk">
	</div>
	<button class="btn btn-success" name="cari"><span class="glyphicon glyphicon-search"</span></button>
</form>

<?php 
$hasil = array();
if(isset($_POST["cari"]))
{
	$hasil = $produk->cari_harga_produk($_POST['kategori'], $_POST['nama_produk']);
}

?>

<?php if ($hasil!=="gagal"): ?>
	<h2>Daftar Pencarian</h2>
	<div class="row">
		<?php foreach ($hasil as $key => $value): ?>
			<?php $data = $produk->ambil_produk_penawaran($value['ID']); ?>
			<div class="col-md-4">
				<div class="thumbnail">
					<a href="">
						<img src="../asset/img/produk/<?php echo $value['Gambar1']; ?>">
					</a>
					<div class="caption">
						<?php if (isset($data['Nama_Hasil'])): ?>
							<h3><?php echo $data['Nama_Hasil']; ?></h3>
						<?php endif ?>
						<?php if (isset($data['Nama_Alat'])): ?>
							<h3><?php echo $data['Nama_Alat']; ?></h3>
						<?php endif ?>
						<?php if (isset($data['Nama_Bahan'])): ?>
							<h3><?php echo $data['Nama_Bahan']; ?></h3>
						<?php endif ?>
						<span class="label label-danger"><?php echo $value['Harga']; ?></span>

					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>
<?php if($hasil=="gagal"): ?>
	<br>
	<div class="alert alert-danger">Pencarian Produk tidak ditemukan</div>
<?php endif ?>