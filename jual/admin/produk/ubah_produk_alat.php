<?php 

$id_alat = $_GET['kode'];

$ambil = $produk->ambil_produk_alat($id_alat);

 ?>

<h2>Ubah Produk</h2>
<form method="post" enctype="multipart/form-data">
<br>
 <div class="form-group">
			<label>Kode Alat</label>
			<input type="text"  class="form-control" name="id_alat" value=" <?php echo $ambil['ID_Alat']; ?>" readonly></ins>
		</div>
		<div class="form-group">
			<label>Nama Alat</label>
			<input type="text"  class="form-control" name="nama_alat" value=" <?php echo $ambil['Nama_Alat']; ?>"></ins>
		</div>
		<div class="form-group">
			<label>Deskripsi Alat</label>
			<textarea class="form-control" name="deskripsi_alat" required=""> <?php echo $ambil['Deskripsi_Alat']; ?></textarea>
		</div>
		<div class="form-group">
			<label>Spesifikasi</label>
			<textarea class="form-control" name="spesifikasi" required=""> <?php echo $ambil['Spesifikasi']; ?></textarea>
		</div>
		<div class="form-group">
			<label>Harga Terendah</label>
			<input type="text"  class="form-control" name="harga_terendah" value=" <?php echo $ambil['Harga_Terendah']; ?>"></ins>
		</div>
		<div class="form-group">
			<label>Harga Tertinggi</label>
			<input type="text"  class="form-control" name="harga_tertinggi" value=" <?php echo $ambil['Harga_Tertinggi']; ?>"></ins>
		</div>
		<div class="form-group">
			<label>Fungsi</label>
			<textarea class="form-control" name="fungsi" required=""> <?php echo $ambil['Fungsi']; ?></textarea>
		</div>
		<button class="btn btn-warning" name="simpan_alat">Simpan</button>
</form>

<?php 
if(isset($_POST["simpan_alat"]))
{
	$produk->ubah_produk_alat($_POST['nama_alat'], $_POST['deskripsi_alat'],$_POST['spesifikasi'],$_POST['harga_terendah'],$_POST['harga_tertinggi'],$_POST['fungsi'], $id_alat);

	echo "<script>alert('Data berhasil diubah'); location='index.php?halaman=produk'</script>";
}

 ?>


