<?php 
error_reporting(0);
$data_penawaran = $produk->tampil_penawaran_produk();

?>
<h2>Daftar Penawaran Produk</h2>
<table class="table table-bordered table-striped" id="thetable">
	<thead>
		<tr>
			<th>NO</th>
			<th>NAMA PRODUK</th>
			<th>SPESIFIKASI</th>
			<th>KONDISI</th>
			<th>MERK</th>
			<th>HARGA</th>
			<th>TAHUN PRODUKSI</th>
			<th>STOK</th>
			<th>SATUAN </th>
			<th>STATUS</th>
			<th class="text-center">VALIDASI</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($data_penawaran as $key => $value):
			?>
		<?php $data_produk = $produk->ambil_penawaran_produk($value['ID_Produk']); ?>
		<?php $hasil_produk = $produk->ambil_produk_penawaran($data_produk['ID']); ?>
		<tr>
			<td> <?php echo $key+1; ?></td>
			<td>
				<?php if ($hasil_produk['Nama_Hasil']!==""): ?>
					<?php echo $hasil_produk['Nama_Hasil']; ?>
				<?php endif ?>
				<?php if ($hasil_produk['Nama_Alat']!==""): ?>
					<?php echo $hasil_produk['Nama_Alat']; ?>
				<?php endif ?>
				<?php if ($hasil_produk['Nama_Bahan']!==""): ?>
					<?php echo $hasil_produk['Nama_Bahan']; ?>
				<?php endif ?>
			</td>
			<td> <?php echo $value['Spesifikasi_Barang']; ?></td>
			<td> <?php echo $value['Kondisi_Barang']; ?></td>
			<td> <?php echo $value['Merk']; ?></td>
			<td> <?php echo $hasil_produk['Harga']; ?></td>
			<td> <?php echo $value['Tahun_Produksi']; ?></td>
			<td> <?php echo $value['Stok']; ?></td>
			<td> <?php echo $data_produk['Satuan_Barang']; ?></td>
			<td><center> <?php echo $value['Status']; ?></center> </td>
			<td>
				<center>
					<?php if ($value['Status']=="Aktif"): ?>
					<?php else: ?>
						<a href="index.php?halaman=status_produk_penjual&kode=<?php echo $value['ID_Penawaran']; ?>&code=<?php echo $value['ID_User']; ?>" class="btn btn-primary">Aktif</a>
					<?php endif ?>
				</center>
			</td>
		</tr>
	<?php endforeach ?>
</tbody>
</table>