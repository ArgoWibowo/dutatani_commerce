<?php 

$id_produk = $_GET['kode'];

$ambil =$produk->ambil_hasil_produk($id_produk);

$data_spesies = $spesies->tampil_spesies();

?>
<h2>Ubah Produk</h2>
<form method="post" enctype="multipart/form-data">
	<br>
	<div>
		<label>Kode Hasil</label>
		<input type="text" class="form-control" name="ID_Hasil" required="" value="<?php echo $ambil['ID_Hasil']; ?>" readonly>
	</div>
	<br>
	<div>
		<label>Nama Hasil</label>
		<input type="text" class="form-control" name="Nama_Hasil" required="" value="<?php echo $ambil['Nama_Hasil']; ?>">
	</div>
	<br>

	<div>
		<label>Spesies</label>
		<select class="form-control" name="ID_Spesies">
			<option value="">--Pilih Spesies--</option>
			<?php foreach ($data_spesies as $X => $Y): ?>
				<option value="<?php echo $Y['ID_Spesies'] ?>" <?php if($ambil['ID_Spesies']== $Y['ID_Spesies']) {echo "selected";} ?>><?php echo $Y['Nama_Tanaman']; ?></option>
			<?php endforeach ?>
		</select>
	</div>
	<br>
	<div>
		<label>Deskripsi Hasil</label>
		<textarea class="form-control" name="Deskripsi_Hasil" required=""><?php echo $ambil['Deskripsi_Hasil']; ?></textarea>
	</div>
	<br>
	<div>
		<label>Harga Terendah</label>
		<input type="number" class="form-control" name="Harga_Terendah" required="" value="<?php echo $ambil['Harga_Terendah']; ?>">
	</div>
	<br>
	<div>
		<label>Harga Tertinggi</label>
		<input type="number" class="form-control" name="Harga_Tertinggi" required="" value="<?php echo $ambil['Harga_Tertinggi']; ?>">
	</div>
	<br>
	<div>
		<label>Satuan</label>
		<input type="text" class="form-control" name="Satuan" required="" value="<?php echo $ambil['Satuan']; ?>">
	</div>
	<br>
	<button class="btn btn-warning" name="simpan">Simpan</button>
</form>

<?php 
//jika ada tombol di dalam form dengan nama= simpan

if(isset($_POST["simpan"]))
{
	//variabel yang menyimpan class produk menjalankan function ubah produk
	$produk->ubah_hasil_produk($_POST['Nama_Hasil'], $_POST['ID_Spesies'], $_POST['Deskripsi_Hasil'], $_POST['Harga_Terendah'], $_POST['Harga_Tertinggi'], $_POST['Satuan'], $id_produk);

	//menampilkan pesan layar
	echo "<script>alert('Data berhasil diubah'); location='index.php?halaman=produk'</script>";
}



?>
