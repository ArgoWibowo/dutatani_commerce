<?php 

$semuahasil = $admin->tampil_hasil_produk();

$semuaalat = $admin->tampil_produk_alat();

$semuabahan = $admin->tampil_produk_bahan();

// echo "<pre>";
// print_r($semuaproduk);
// echo "</pre>";

?>


<h2>Daftar Produk</h2>
<!-- <a href="index.php?halaman=tambah_produk" class="btn btn-primary">Tambah Produk</a>
	<hr> -->
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#produk">Produk Hasil Tani</a></li>
		<li><a data-toggle="tab" href="#alat">Produk ALat Tani</a></li>
		<li><a data-toggle="tab" href="#bahan">Produk Bahan Tani</a></li>
	</ul>

	<div class="tab-content">
		<div id="produk" class="tab-pane fade in active">
			<h3>Produk Hasil Tani</h3>
			<table class="table table-bordered table-striped" id="thetable">
				<thead>
					<tr>
						<th>NO</th>
						<th>KODE HASIL</th>
						<th>NAMA HASIL</th>
						<th>SPESIES</th>
						<th>DESKRIPSI HASIL</th>
						<th>HARGA TERENDAH</th>
						<th>HARGA TERTINGGI</th>
						<th>MASA EXP</th>
						<th>HARGA JUAL</th>
						<th>SATUAN</th>
						<!-- <th class="text-center">AKSI</th>
						<th class="text-center">AKSI</th> -->
					</tr>
				</thead>
				<tbody>
					<?php foreach ($semuahasil as $key => $value): ?>
						<tr>
							<td> <?php echo $key+1; ?></td>
							<td> <?php echo $value['ID_Hasil']; ?></td>
							<td> <?php echo $value['Nama_Hasil']; ?></td>
							<td> <?php echo $value['Nama_Tanaman']; ?></td>
							<td> <?php echo $value['Deskripsi_Hasil']; ?></td>
							<td> <?php echo $value['Harga_Terendah']; ?></td>
							<td> <?php echo $value['Harga_Tertinggi']; ?></td>
							<td> <?php echo $value['Masa_Expayet']; ?></td>
							<td> <?php echo $value['Harga']; ?></td>
							<td> <?php echo $value['Satuan']; ?></td>
							<!-- <td> 
								<!-- <center><a href="index.php?halaman=ubah_produk&kode=<?php echo $value['ID_Hasil']; ?>" class="btn btn-warning">Ubah</a></center> -->
							<!-- </td> --> 
							<!-- <td>
								<!-- <center><a href="index.php?halaman=hapus_produk&kode=<?php echo $value['ID_Hasil']; ?>" class="btn btn-danger">Hapus</a></center> -->

							<!-- </td> --> 
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
		<div id="alat" class="tab-pane fade">
			<h3>Produk Alat Tani</h3>
			<table class="table table-bordered table-striped" id="thetable2">
				<thead>
					<tr>
						<th>NO</th>
						<th>KODE ALAT</th>
						<th>NAMA ALAT</th>
						<th>DESKRIPSI ALAT</th>
						<th>SPESIFIKASI</th>
						<th>HARGA TERENDAH</th>
						<th>HARGA TERTINGGI</th>
						<th>HARGA JUAL</th>
						<th>FUNGSI</th>
						<!-- <th class="text-center">AKSI</th>
						<th class="text-center">AKSI</th> -->
					</tr>
				</thead>
				<tbody>
					<?php foreach ($semuaalat as $key => $value): ?>
						<tr>
							<td> <?php echo $key+1; ?></td>
							<td> <?php echo $value['ID_Alat']; ?></td>
							<td> <?php echo $value['Nama_Alat'] ;?></td>
							<td> <?php echo $value['Deskripsi_Alat']; ?></td>
							<td> <?php echo $value['Spesifikasi']; ?></td>
							<td> <?php echo $value['Harga_Terendah']; ?></td>
							<td> <?php echo $value['Harga_Tertinggi'] ;?></td>
							<td> <?php echo $value['Harga'] ;?></td>
							<td> <?php echo $value['Fungsi']; ?></td>
							<!-- <td> 
								<!-- <center><a href="index.php?halaman=ubah_produk_alat&kode=<?php echo $value['ID_Alat']; ?>" class="btn btn-warning">Ubah</a></center> -->
							<!-- </td> --> 
							<!-- <td> -->
								<!-- <center><a href="index.php?halaman=hapus_produk_alat&kode=<?php echo $value['ID_Alat']; ?>" class="btn btn-danger">Hapus</a></center>
 -->
							<!-- </td> -->
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
		<div id="bahan" class="tab-pane fade">
			<h3>Produk Bahan Tani</h3>
			<table class="table table-bordered table-striped" id="thetable3">
				<thead>
					<tr>
						<th>NO</th>
						<th>KODE BAHAN</th>
						<th>NAMA BAHAN</th>
						<th>DESKRIPSI BAHAN</th>
						<th>SPESIFIKASI BAHAN</th>
						<th>HARGA TERENDAH</th>
						<th>HARGA TERTINGGI</th>
						<th>HARGA JUAL</th>
						<th>FUNGSI BAHAN</th>
						<th>JENIS BAHAN</th>
						<!-- <th class="text-center">AKSI</th>
						<th class="text-center">AKSI</th> -->
					</tr>
				</thead>
				<tbody>
					<?php foreach ($semuabahan as $key => $value): ?>
						<tr>
							<td> <?php echo $key+1; ?></td>
							<td> <?php echo $value['ID_Bahan']; ?></td>
							<td> <?php echo $value['Nama_Bahan']; ?></td>
							<td> <?php echo $value['Deskripsi_Bahan']; ?></td>
							<td> <?php echo $value['Spesifikasi_Bahan']; ?></td>
							<td> <?php echo $value['Harga_Terendah']; ?></td>
							<td> <?php echo $value['Harga_Tertinggi']; ?></td>
							<td> <?php echo $value['Harga'] ;?></td>
							<td> <?php echo $value['Fungsi_Bahan']; ?></td>
							<td> <?php echo $value['Jenis_Bahan']; ?></td>
							<!-- <td>  -->
								<!-- <center><a href="index.php?halaman=ubah_produk_bahan&kode=<?php echo $value['ID_Bahan']; ?>" class="btn btn-warning">Ubah</a></center> -->
							<!-- </td> -->
							<!-- <td> -->
								<!-- <center><a href="index.php?halaman=hapus_produk_bahan&kode=<?php echo $value['ID_Bahan']; ?>" class="btn btn-danger">Hapus</a></center> -->
							<!-- </td> -->
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>