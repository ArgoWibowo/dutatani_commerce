<?php 
//jika ada parameter status_kirim di url maka, 
$datakategori =  $kategori->tampil_kategori();
$data_penjual = $penjual->tampil_penjual();

if (isset($_GET['status_kirim'])) 
{
	$pembeli->ubah_status_kirim($_GET['status_kirim']);
	echo "<script>alert('Konfirmasi Pengiriman Berhasil');</script>";
	echo "<script>location='index.php?halaman=permintaan';</script>";
}

$data_trans = $pembeli->tampil_transaksi_beli_penjual()

?>
<h2>Laporan Transaksi Penjualan</h2>
<form method="post" class="form-inline hidden-print">
	<input type="date" id="tgl1" name="date1" class="form-control">  s.d
	<input type="date" id="tgl2" name="date2" class="form-control">
	<select class="form-control" name="sorting_kat">
		<option value="">--Kategori--</option>
		<?php foreach ($datakategori as $key => $value): ?>
				<option value="<?php echo $value['ID_Kategori'];?>"> <?php echo $value['Nama_kategori']; ?></option>
			<?php endforeach ?>	
	</select>
	<select class="form-control" name="sorting_penjual">
		<option value="">--Penjual--</option>
		<?php foreach ($data_penjual as $key => $value): ?>
				<option value="<?php echo $value['ID_User'];?>"> <?php echo $value['nama']; ?></option>
			<?php endforeach ?>	
	</select>
	<button class="btn btn-success" name="cari"><span class="glyphicon glyphicon-search"</span></button>
</form>
<?php
if(isset($_POST['cari']))
{
	$data_trans = $pembeli->cari_transaksi_penjual($_POST['date1'], $_POST['date2'], $_POST['sorting_kat'], $_POST['sorting_penjual']);
}
elseif (isset($_POST['semua_trans'])) 
{
	$data_trans = $pembeli->tampil_transaksi_beli_penjual();
}
?>
<br>
<strong>Total Transaksi : <?php echo count($data_trans); ?></strong>
<br>
<table class="table table-bordered table-striped" id="#">
	<thead>
		<tr>
			<th>NO</th>
			<th>KODE PEMBELI</th>
			<th>NO INVOICE</th>
			<th>QTY</th>
			<th>HARGA</th>
			<th>TANGGAL KEBUTUHAN</th>
			<th>TANGGAL PERMINTAAN</th>
			<th>EKPEDISI</th>
			<th>NO RESI</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data_trans as $key => $value): ?>
			<tr>
				<td> <?php echo $key+1; ?></td>
				<td> <?php echo $value['ID_User']; ?></td>
				<td> <?php echo $value['ID_Permintaan']; ?></td>
				<td> <?php echo $value['Qty']; ?></td>
				<td> <?php echo $value['Harga']; ?></td>
				<td> <?php echo $value['Tgl_Kebutuhan']; ?></td>
				<td> <?php echo $value['Tgl_Permintaan']; ?></td>
				<td> <?php echo $value['Nama_Ekspedisi']; ?></td>
				<td> <?php echo $value['No_Resi']; ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
<a onclick="window.print()" class="btn btn-success hidden-print">Cetak And Save</a>