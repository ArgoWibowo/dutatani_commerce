<?php 

$data_penjual = $penjual->tampil_penjual();

?>

<h2>Daftar Penjual</h2>
<table class="table table-bordered table-striped" id="thetable">
	<thead>
		<tr>
			<th>NO</th>
			<th>KODE PENJUAL</th>
			<th>NAMA PENJUAL</th>
			<th>ALAMAT</th>
			<th>NO TELEPON</th>
			<th>EMAIL</th>
			<th><center>STATUS</center></th>
			<th class="text-center">AKSI</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data_penjual as $key => $value): ?>
			<tr>
				<td> <?php echo $key+1; ?></td>
				<td> <?php echo $value['ID_User'] ?></td>
				<td> <?php echo $value['nama']; ?></td>
				<td> <?php echo $value['alamat']; ?></td>
				<td> <?php echo $value['nomor_telpon']; ?></td>
				<td> <?php echo $value['Email']; ?></td>
				<td><center> <?php echo $value['Status_Akun']; ?></center></td>
				<!-- <td> 
					<a href="" class="btn btn-warning">Ubah</a>
				</td>
				<td>
					<a href="" class="btn btn-danger">Hapus</a>
				</td> -->
				<td>
					<?php if ($value['Status_Akun']=="Aktif"): ?>
					<?php else: ?>
						<center><a href="index.php?halaman=status_registrasi&kode=<?php echo $value['ID_User']; ?>" class="btn btn-primary">Validasi</a></center>
					<?php endif ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>