<?php 


$data_pembayaran = $pembeli->tampil_pembayaran($_GET['kode']);
$data_permintaan = $pembeli->ambil_permintaan($_GET['kode']);
$data_penjual = $produk->ambil_penawaran($data_permintaan['ID_Penawaran']);
$data_bukti = $admin->ambil_bukti_transfer($_GET['kode']);

?>

<h2>Data Pembayaran</h2>

<?php if (empty($data_pembayaran)): ?>
	<div class="alert alert-danger">
		Belum ada bukti pembayaran yang masuk
	</div>
<?php else: ?>
	<div class="row">
		<div class="col-md-6"> 
			<img src="../asset/img/bukti/<?php echo $data_pembayaran['Bukti_Pembayaran']; ?>" class="img-square img-responsive" data-toggle="modal" data-target="#myModal"></div>
			<div class="col-md-6">
				<table class="table">
					<tr>
						<th>KODE PEMBAYARAN</th>
						<td> <?php echo $data_pembayaran['ID_Pembayaran']; ?></td>
					</tr>
					<tr>
						<th>KODE PERMINTAAN</th>
						<td> <?php echo $data_pembayaran['ID_Permintaan']; ?></td>
					</tr>
					<tr>
						<th>KODE USER</th>
						<td> <?php echo $data_pembayaran['ID_User']; ?></td>
					</tr>
					<tr>
						<th>NO REKENING</th>
						<td> <?php echo $data_pembayaran['No_Rekening']; ?></td>
					</tr>
					<tr>
						<th>NAMA PEMILIK</th>
						<td> <?php echo $data_pembayaran['Nama_Rekening']; ?></td>
					</tr>
					<tr>
						<th>TOTAL BAYAR</th>
						<td> <?php echo $data_pembayaran['Jumlah_Pembayaran']; ?></td>
					</tr>
				</table>
				<?php if ($data_permintaan['Status']=="Sudah Bayar"): ?>
					<?php if (empty($data_bukti)): ?>
						<a href="index.php?halaman=kirim_bukti&kode=<?php echo $data_permintaan['ID_Permintaan']; ?>&code=<?php echo $data_penjual['ID_User']; ?>" class="btn btn-success">Kirim Bukti Ke Penjual</a>
					<?php endif ?>
				<?php else: ?>
					<a href="index.php?halaman=status_permintaan&kode=<?php echo $data_permintaan['ID_Permintaan']; ?>&code=<?php echo $data_permintaan['ID_User']; ?>" class="btn btn-primary">Validasi</a>
				<?php endif ?>
			</div>
		</div>
	<?php endif ?>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<center><img src="../asset/img/bukti/<?php echo $data_pembayaran['Bukti_Pembayaran']; ?>" class="img-responsive"></center>
				</div>
			</div>
		</div>
	</div>