							<?php $data_penawaran = $produk->tampil_penawaran_produk_aktif(); ?>
							<br>
							<section class="produk">
								<div class="container">
									<div class="row">
										<div class="col-md-4">
											<h2>Kategori</h2>
											<?php $data_kategori=$kategori->tampil_kategori(); ?>
											<div class="list-group">
												<?php foreach ($data_kategori as $key => $value): ?>	
													<a href="index.php?halaman=kategori_produk&id=<?php echo $value['ID_Kategori']; ?>" class="list-group-item"><?php echo $value['Nama_kategori'] ?></a>
												<?php endforeach ?>
											</div>
										</div>
										<div class="col-md-8">
											<h2>Semua Produk</h2>
											<div class="row">
												<?php foreach ($data_penawaran as $key => $value): ?>
													<?php $data = $produk->ambil_produk_penawaran($value['ID']); ?>
													<div class="col-md-4">
														<div class="thumbnail">
															<div class="owl-carousel">
																<div>
																	<img src="asset/img/produk/<?php echo $value['Gambar1']; ?>" width="200" height="200">
																</div>
																<div>
																	<img src="asset/img/produk/<?php echo $value['Gambar2']; ?>" width="200" height="200">
																</div>
															</div>
															<div class="caption">
																<?php if (isset($data['Nama_Hasil'])): ?>
																	<h6><?php echo $data['Nama_Hasil']; ?></h6>
																<?php endif ?>
																<?php if (isset($data['Nama_Alat'])): ?>
																	<h6><?php echo $data['Nama_Alat']; ?></h6>
																<?php endif ?>
																<?php if (isset($data['Nama_Bahan'])): ?>
																	<h6><?php echo $data['Nama_Bahan']; ?></h6>
																<?php endif ?>
																<span class="label label-danger"><?php echo $value['Harga']; ?></span>
																<br>
																<br>
																<a href="" class="btn btn-primary btn-sm">Beli</a>
																<a href="" class="btn btn-info btn-sm">Detail</a>
															</div>
														</div>
													</div>
												<?php endforeach ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<br>
				<br>